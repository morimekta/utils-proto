package net.morimekta.proto.testing.test;

import com.google.protobuf.ByteString;
import com.google.protobuf.Message;
import net.morimekta.collect.UnmodifiableSet;
import net.morimekta.proto.testing.matchers.EqualMessageIgnoring;
import net.morimekta.proto.testing.test.TestProto.CompactMessage;
import net.morimekta.proto.testing.test.TestProto.DefaultMessage;
import net.morimekta.proto.testing.test.TestProto.NormalMessage;
import net.morimekta.proto.testing.test.TestV2Proto.SimpleMessage;
import org.hamcrest.StringDescription;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Map;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.proto.testing.ProtoMatchers.equalToMessage;
import static net.morimekta.proto.testing.ProtoMatchers.equalToMessageIgnoreExtensions;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ProtoMatchersTest {
    @Test
    public void testIsEqualMessage() {
        assertThat(DefaultMessage.newBuilder()
                                 .setStr("foo")
                                 .setNormal(NormalMessage.newBuilder()
                                                         .setL(4L)
                                                         .build())
                                 .addIntegers(42)
                                 .addNormals(NormalMessage.newBuilder()
                                                          .setI(21)
                                                          .build())
                                 .putScMap("bla", CompactMessage.newBuilder()
                                                                .setI(2)
                                                                .build())
                                 .build(),
                   equalToMessage(
                           DefaultMessage.newBuilder()
                                         .setStr("bar")
                                         .setNormal(NormalMessage.newBuilder()
                                                                 .setL(2L)
                                                                 .build())
                                         .addIntegers(42)
                                         .addNormals(NormalMessage.newBuilder()
                                                                  .setI(21)
                                                                  .build())
                                         .putScMap("bla", CompactMessage.newBuilder()
                                                                        .setI(4)
                                                                        .build())
                                         .build(),
                           "str",
                           "normal.l",
                           "sc_map.i"));

        assertThat(DefaultMessage.newBuilder()
                                 .setCompact(CompactMessage.newBuilder()
                                                           .build())
                                 .putAllLiMap(Map.of(1L, 2))
                                 .addIntegers(42)
                                 .addNormals(NormalMessage.newBuilder()
                                                          .setI(21)
                                                          .build())
                                 .build(),
                   is(equalToMessage(
                           DefaultMessage.newBuilder()
                                         .setCompact(CompactMessage.newBuilder()
                                                                   .build())
                                         .putAllLiMap(Map.of(1L, 2))
                                         .addIntegers(42)
                                         .addNormals(NormalMessage.newBuilder()
                                                                  .setI(12)
                                                                  .build())
                                         .build(),
                           "normals.i")));

        assertThat(SimpleMessage.newBuilder()
                                .setI(42)
                                .setExtension(TestExtProto.anExtension, "foo")
                                .build(),
                   is(equalToMessage(
                           SimpleMessage.newBuilder()
                                        .setI(42)
                                        .setExtension(TestExtProto.anExtension, "foo")
                                        .build(),
                           "l")));

        assertThat(SimpleMessage.newBuilder()
                                .setI(42)
                                .setExtension(TestExtProto.anExtension, "foo")
                                .build(),
                   is(equalToMessageIgnoreExtensions(
                           SimpleMessage.newBuilder()
                                        .setI(42)
                                        .setExtension(TestExtProto.anExtension, "bar")
                                        .build())));

        try {
            assertThat("True-foo",
                       DefaultMessage.newBuilder()
                                     .setBoolean(true)
                                     .build(),
                       equalToMessage(DefaultMessage.newBuilder()
                                                    .setStr("foo")
                                                    .build()));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("True-foo\n" +
                          "Expected: test.DefaultMessage{\n" +
                          "  str = \"foo\"\n" +
                          "}\n" +
                          "     but: was test.DefaultMessage{\n" +
                          "  boolean = true\n" +
                          "}"));
        }

        try {
            assertThat("Foo-barren",
                       DefaultMessage.newBuilder()
                                     .setStr("foo")
                                     .setDbl(3.7)
                                     .build(),
                       equalToMessage(DefaultMessage.newBuilder()
                                                    .setStr("bar")
                                                    .setLong(42)
                                                    .build(),
                                      "str"));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("Foo-barren\n" +
                          "Expected: test.DefaultMessage{\n" +
                          "  long = 42\n" +
                          "}\n" +
                          "     but: was test.DefaultMessage{\n" +
                          "  dbl = 3.7\n" +
                          "}"));
        }

        try {
            assertThat("Nelly",
                       null,
                       equalToMessage(DefaultMessage.newBuilder().build(),
                                      "str"));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("Nelly\n" +
                          "Expected: test.DefaultMessage{}\n" +
                          "     but: was null"));
        }

        try {
            assertThat("Foo-only",
                       NormalMessage.newBuilder().build(),
                       equalToMessage(DefaultMessage.newBuilder().build(),
                                      "compact.l"));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("Foo-only\n" +
                          "Expected: test.DefaultMessage{}\n" +
                          "     but: was test.NormalMessage{}"));
        }
        try {
            assertThat("Integers-len",
                       DefaultMessage.newBuilder()
                                     .addIntegers(12)
                                     .build(),
                       equalToMessage(DefaultMessage.newBuilder()
                                                    .addIntegers(12)
                                                    .addIntegers(42)
                                                    .build(),
                                      "str"));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("Integers-len\n" +
                          "Expected: test.DefaultMessage{\n" +
                          "  integers = [12, 42]\n" +
                          "}\n" +
                          "     but: was test.DefaultMessage{\n" +
                          "  integers = [12]\n" +
                          "}"));
        }
        try {
            assertThat("Integers-content",
                       DefaultMessage.newBuilder()
                                     .addIntegers(12)
                                     .build(),
                       equalToMessage(
                               DefaultMessage.newBuilder()
                                             .addIntegers(42)
                                             .build(),
                               "str"));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("Integers-content\n" +
                          "Expected: test.DefaultMessage{\n" +
                          "  integers = [42]\n" +
                          "}\n" +
                          "     but: was test.DefaultMessage{\n" +
                          "  integers = [12]\n" +
                          "}"));
        }

        try {
            assertThat("LI-size",
                       DefaultMessage.newBuilder()
                                     .putLiMap(12, 14)
                                     .build(),
                       equalToMessage(DefaultMessage.newBuilder()
                                                    .putLiMap(42, 88)
                                                    .build(),
                                      "str"));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("LI-size\n" +
                          "Expected: test.DefaultMessage{\n" +
                          "  li_map = {\n" +
                          "    42: 88\n" +
                          "  }\n" +
                          "}\n" +
                          "     but: was test.DefaultMessage{\n" +
                          "  li_map = {\n" +
                          "    12: 14\n" +
                          "  }\n" +
                          "}"));
        }
        try {
            assertThat("LI-content",
                       DefaultMessage.newBuilder()
                                     .putLiMap(12, 14)
                                     .build(),
                       equalToMessage(DefaultMessage.newBuilder()
                                                    .putLiMap(12, 88)
                                                    .build(),
                                      "str"));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("LI-content\n" +
                          "Expected: test.DefaultMessage{\n" +
                          "  li_map = {\n" +
                          "    12: 88\n" +
                          "  }\n" +
                          "}\n" +
                          "     but: was test.DefaultMessage{\n" +
                          "  li_map = {\n" +
                          "    12: 14\n" +
                          "  }\n" +
                          "}"));
        }
        try {
            assertThat("Ext-content",
                       SimpleMessage.newBuilder()
                                    .setExtension(TestExtProto.anExtension, "foo")
                                    .build(),
                       equalToMessage(
                               SimpleMessage.newBuilder()
                                            .setExtension(TestExtProto.anExtension, "bar")
                                            .build(),
                               "l"));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("Ext-content\n" +
                          "Expected: test.v2.SimpleMessage{\n" +
                          "  (test.ext.an_extension) = \"bar\"\n" +
                          "}\n" +
                          "     but: was test.v2.SimpleMessage{\n" +
                          "  (test.ext.an_extension) = \"foo\"\n" +
                          "}"));
        }

        try {
            assertThat("Ext-ignored",
                       SimpleMessage.newBuilder()
                                    .setI(42)
                                    .setExtension(TestExtProto.anExtension, "foo")
                                    .build(),
                       equalToMessageIgnoreExtensions(
                               SimpleMessage.newBuilder()
                                            .setI(43)
                                            .setExtension(TestExtProto.anExtension, "bar")
                                            .build()));
        } catch (AssertionError e) {
            assertThat(e.getMessage(),
                       is("Ext-ignored\n" +
                          "Expected: test.v2.SimpleMessage{\n" +
                          "  i = 43\n" +
                          "}\n" +
                          "     but: was test.v2.SimpleMessage{\n" +
                          "  i = 42\n" +
                          "}"));
        }
    }

    public static Stream<Arguments> argsDescribeTo() {
        return Stream.of(
                arguments(
                        DefaultMessage
                                .newBuilder()
                                .setBoolean(true)
                                .setInt(11)
                                .setLong(14)
                                .setDbl(3.7)
                                .setBin(ByteString.copyFromUtf8("bar"))
                                .setStr("foo")
                                .setCompact(CompactMessage.newBuilder()
                                                          .setI(4)
                                                          .setL(2)
                                                          .build())
                                .addAllIntegers(List.of(1, 3, 5, 7))
                                .addAllNormals(List.of(NormalMessage.newBuilder()
                                                                    .setI(42)
                                                                    .build(),
                                                       NormalMessage.newBuilder()
                                                                    .setL(72)
                                                                    .build()))
                                .putScMap("boo", CompactMessage.newBuilder()
                                                               .setI(33)
                                                               .build())
                                .putScMap("wee", CompactMessage.newBuilder()
                                                               .setI(66)
                                                               .build())
                                .build(),
                        "test.DefaultMessage{\n" +
                        "  boolean = true\n" +
                        "  int = 11\n" +
                        "  long = 14\n" +
                        "  dbl = 3.7\n" +
                        "  str = \"foo\"\n" +
                        "  bin = b64(YmFy)\n" +
                        "  compact = {\n" +
                        "    l = 2\n" +
                        "    i = 4\n" +
                        "  }\n" +
                        "  integers = [1, 3, 5, 7]\n" +
                        "  normals = [\n" +
                        "    {\n" +
                        "      i = 42\n" +
                        "    },\n" +
                        "    {\n" +
                        "      l = 72\n" +
                        "    }\n" +
                        "  ]\n" +
                        "  sc_map = {\n" +
                        "    \"boo\": {\n" +
                        "      i = 33\n" +
                        "    },\n" +
                        "    \"wee\": {\n" +
                        "      i = 66\n" +
                        "    }\n" +
                        "  }\n" +
                        "}",
                        listOf("sc_map.l")),
                arguments(
                        DefaultMessage.newBuilder()
                                      .setStr("foo")
                                      .setDbl(3.7)
                                      .setNormal(NormalMessage.newBuilder()
                                                              .setI(3)
                                                              .setL(7)
                                                              .build())
                                      .build(),
                        "test.DefaultMessage{\n" +
                        "  str = \"foo\"\n" +
                        "  normal = {\n" +
                        "    l = 7\n" +
                        "  }\n" +
                        "}",
                        listOf("dbl", "normal.i")),
                arguments(
                        DefaultMessage.newBuilder()
                                      .setDbl(3.7)
                                      .setStr("foo")
                                      .build(),
                        "test.DefaultMessage{\n" +
                        "  dbl = 3.7\n" +
                        "  str = \"foo\"\n" +
                        "}",
                        listOf("normal.i")),
                arguments(
                        SimpleMessage.newBuilder()
                                     .setI(43)
                                     .setExtension(TestExtProto.anExtension, "bar")
                                     .build(),
                        "test.v2.SimpleMessage{\n" +
                        "  i = 43\n" +
                        "  (test.ext.an_extension) = \"bar\"\n" +
                        "}",
                        listOf()));
    }

    @ParameterizedTest
    @MethodSource("argsDescribeTo")
    public void testDescribeTo(Message message, String expected, List<String> ignore) {
        var sd = new StringDescription();
        new EqualMessageIgnoring<>(message, false, UnmodifiableSet.asSet(ignore)).describeTo(sd);
        assertThat(sd.toString(), is(expected));
    }
}
