module net.morimekta.proto.testing.test {
    requires com.google.protobuf;
    requires net.morimekta.collect;
    requires net.morimekta.proto.testing;
    requires net.morimekta.proto;
    requires org.hamcrest;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
    requires org.junit.jupiter.params;

    exports net.morimekta.proto.testing.test;
}