/**
 * Module with gson adapters for proto types.
 */
module net.morimekta.proto.testing {
    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires net.morimekta.proto;
    requires org.hamcrest;
    requires com.google.protobuf;

    exports net.morimekta.proto.testing;
    exports net.morimekta.proto.testing.matchers;
}