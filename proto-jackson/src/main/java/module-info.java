import net.morimekta.proto.jackson.ProtoModule;

/**
 * Proto message and enum serializers and deserializers for jackson.
 */
module net.morimekta.proto.jackson {
    requires net.morimekta.proto;
    requires net.morimekta.collect;
    requires net.morimekta.strings;

    requires com.google.protobuf;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.core;

    exports net.morimekta.proto.jackson;
    exports net.morimekta.proto.jackson.sio;

    opens net.morimekta.proto.jackson to com.fasterxml.jackson.databind;
    opens net.morimekta.proto.jackson.adapter to com.fasterxml.jackson.databind;

    provides com.fasterxml.jackson.databind.Module with
            ProtoModule;
}