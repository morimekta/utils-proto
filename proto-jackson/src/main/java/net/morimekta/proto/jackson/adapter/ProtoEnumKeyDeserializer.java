/*
 * Copyright 2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.jackson.adapter;

import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.KeyDeserializer;
import com.google.protobuf.ProtocolMessageEnum;
import net.morimekta.proto.ProtoEnum;

import java.util.regex.Pattern;

/**
 * Deserializes proto enums for map keys.
 */
public class ProtoEnumKeyDeserializer
        extends KeyDeserializer {
    private static final Pattern NUMERIC = Pattern.compile("(0|[1-9][0-9]*)");

    private final ProtoEnum<?> descriptor;

    /**
     * Instantiate deserializer.
     *
     * @param descriptor The proto enum descriptor.
     */
    public ProtoEnumKeyDeserializer(ProtoEnum<?> descriptor) {
        this.descriptor = descriptor;
    }

    @Override
    public ProtocolMessageEnum deserializeKey(String key, DeserializationContext deserializationContext) {
        if (NUMERIC.matcher(key).matches()) {
            return descriptor.findByNumber(Integer.parseInt(key));
        } else {
            return descriptor.findByName(key);
        }
    }
}
