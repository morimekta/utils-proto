package net.morimekta.proto.jackson.sio;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Message;
import net.morimekta.proto.jackson.ProtoModule;
import net.morimekta.proto.sio.MessageWriter;

import java.io.IOException;
import java.io.Writer;

import static java.util.Objects.requireNonNull;

/**
 * Writer to use jackson object mapper to write messages.
 */
public class JacksonMessageWriter implements MessageWriter {
    private final Writer       writer;
    private final ObjectMapper mapper;
    private final boolean      pretty;

    /**
     * Create jackson writer with mapper.
     *
     * @param writer Writer to write to.
     * @param mapper The object mapper to use.
     * @param pretty If should do pretty printing.
     */
    public JacksonMessageWriter(Writer writer, ObjectMapper mapper, boolean pretty) {
        this.writer = requireNonNull(writer, "writer == null");
        this.mapper = requireNonNull(mapper, "mapper == null").registerModule(new ProtoModule());
        this.pretty = pretty;
    }

    @Override
    public void write(Message message) throws IOException {
        if (pretty) {
            mapper.writerWithDefaultPrettyPrinter().writeValue(writer, message);
            writer.write("\n");
        } else {
            mapper.writeValue(writer, message);
        }
    }
}
