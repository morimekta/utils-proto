/*
 * Copyright 2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.jackson;

import com.fasterxml.jackson.databind.module.SimpleModule;
import com.google.protobuf.ByteString;
import com.google.protobuf.Message;
import com.google.protobuf.ProtocolMessageEnum;
import net.morimekta.proto.jackson.adapter.ByteStringDeserializer;
import net.morimekta.proto.jackson.adapter.ByteStringKeyDeserializer;
import net.morimekta.proto.jackson.adapter.ByteStringKeySerializer;
import net.morimekta.proto.jackson.adapter.ByteStringSerializer;
import net.morimekta.proto.jackson.adapter.ProtoDeserializers;
import net.morimekta.proto.jackson.adapter.ProtoEnumKeySerializer;
import net.morimekta.proto.jackson.adapter.ProtoEnumSerializer;
import net.morimekta.proto.jackson.adapter.ProtoMessageSerializer;

/**
 * Jackson module registering automatic handling of proto types.
 */
public class ProtoModule
        extends SimpleModule {
    /**
     * Instantiate and register module.
     */
    public ProtoModule() {
        super("morimekta-proto-jackson");
        setDeserializers(new ProtoDeserializers());
        registerSubtypes(ProtocolMessageEnum.class, Message.class);

        addSerializer(ByteString.class, new ByteStringSerializer());
        addDeserializer(ByteString.class, new ByteStringDeserializer());
        addKeySerializer(ByteString.class, new ByteStringKeySerializer());
        addKeyDeserializer(ByteString.class, new ByteStringKeyDeserializer());

        addSerializer(ProtocolMessageEnum.class, new ProtoEnumSerializer());
        addKeySerializer(ProtocolMessageEnum.class, new ProtoEnumKeySerializer());

        addSerializer(Message.class, new ProtoMessageSerializer());
    }
}
