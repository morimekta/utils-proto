/*
 * Copyright 2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.jackson.adapter;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.google.protobuf.ProtocolMessageEnum;
import net.morimekta.proto.ProtoEnum;

import java.io.IOException;
import java.util.Map;

import static com.fasterxml.jackson.databind.DeserializationFeature.READ_UNKNOWN_ENUM_VALUES_AS_NULL;
import static java.util.Objects.requireNonNull;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonEnumMap;

/**
 * Deserialized proto enums.
 *
 * @param <E> The enum type.
 */
public class ProtoEnumDeserializer<E extends Enum<E> & ProtocolMessageEnum>
        extends JsonDeserializer<E> {
    private final ProtoEnum<E>   descriptor;
    private final Map<String, E> jsonNames;

    /**
     * Instantiate deserializer.
     *
     * @param descriptor The proto enum descriptor.
     */
    public ProtoEnumDeserializer(ProtoEnum<E> descriptor) {
        this.descriptor = requireNonNull(descriptor);
        this.jsonNames = getJsonEnumMap(descriptor);
    }

    @Override
    public E deserialize(JsonParser parser, DeserializationContext context)
            throws IOException {
        JsonToken current = parser.currentToken();
        if (current == JsonToken.VALUE_NUMBER_INT) {
            var number = Integer.parseInt(parser.getValueAsString());
            var out = descriptor.findByNumber(Integer.parseInt(parser.getValueAsString()));
            if (out == null && !context.isEnabled(READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                throw JsonMappingException.from(
                        parser, "Unknown " + descriptor.getTypeName() + " enum number " + number);
            }
            return out;
        } else if (current == JsonToken.VALUE_STRING) {
            var name = parser.getValueAsString();
            var out = jsonNames.get(name);
            if (out == null && !context.isEnabled(READ_UNKNOWN_ENUM_VALUES_AS_NULL)) {
                throw JsonMappingException.from(
                        parser, "Unknown " + descriptor.getTypeName() + " enum name \"" + name + "\"");
            }
            return out;
        } else {
            throw JsonMappingException.from(
                    parser, "Invalid " + descriptor.getTypeName() + " enum value token '" + current.asString() + "'");
        }
    }
}
