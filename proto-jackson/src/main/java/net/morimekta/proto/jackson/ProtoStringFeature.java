package net.morimekta.proto.jackson;

import com.fasterxml.jackson.databind.DeserializationConfig;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * Providence specific features or attributes used to configure
 * serialization further. These are technically 'attributes' on
 * the serialization config, but is used are simple feature
 * flags.
 */
public enum ProtoStringFeature {
    /**
     * When serializing Any as unpacked objects, use this field name for the
     * type. Defaults to same as default JsonFormat.
     */
    ANY_TYPE_FIELD_NAME("@type"),
    /**
     * When serializing Any as unpacked objects, use this prefix before the
     * full name of the type. Defaults to same as proto default for Any.
     */
    ANY_TYPE_PREFIX("type.googleapis.com/")
    ;
    private final String defaultValue;

    ProtoStringFeature(String defaultValue) {
        this.defaultValue = defaultValue;
    }

    /**
     * @param mapper The mapper to set value on.
     * @param value The value to be set for this feature.
     * @return The mapper.
     */
    public ObjectMapper set(ObjectMapper mapper, String value) {
        mapper.setConfig(mapper.getSerializationConfig().withAttribute(this, value));
        mapper.setConfig(mapper.getDeserializationConfig().withAttribute(this, value));
        return mapper;
    }

    /**
     * @param sp The serializer provider.
     * @return The feature value, or default if not set.
     */
    public String get(SerializerProvider sp) {
        Object o = sp.getAttribute(this);
        if (o == null) {
            return defaultValue;
        }
        return o.toString();
    }

    /**
     * @param config The deserialization config.
     * @return The feature value, or default if not set.
     */
    public String get(DeserializationConfig config) {
        Object o = config.getAttributes().getAttribute(this);
        if (o == null) {
            return defaultValue;
        }
        return o.toString();
    }
}
