/*
 * Copyright 2017 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.jackson.adapter;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;
import com.google.protobuf.ProtocolMessageEnum;
import net.morimekta.proto.jackson.ProtoFeature;

import java.io.IOException;

import static net.morimekta.proto.utils.JsonNameUtil.getJsonEnumName;

/**
 * Serialize proto enums for map keys.
 */
public class ProtoEnumKeySerializer
        extends StdSerializer<ProtocolMessageEnum> {
    /**
     * Instantiate serializer.
     */
    public ProtoEnumKeySerializer() {
        super(ProtocolMessageEnum.class);
    }

    @Override
    public void serialize(ProtocolMessageEnum value, JsonGenerator gen, SerializerProvider serializers)
            throws IOException {
        if (ProtoFeature.WRITE_ENUM_AS_NUMBER.isEnabled(serializers)) {
            gen.writeFieldName("" + value.getNumber());
        } else {
            gen.writeFieldName(getJsonEnumName(value));
        }
    }
}
