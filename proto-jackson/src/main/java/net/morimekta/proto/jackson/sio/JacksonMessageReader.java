package net.morimekta.proto.jackson.sio;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import net.morimekta.proto.ProtoMessage;
import net.morimekta.proto.jackson.ProtoModule;
import net.morimekta.proto.sio.MessageReader;

import java.io.IOException;
import java.io.Reader;

import static java.util.Objects.requireNonNull;

/**
 * Reader to use jackson object mapper to parse messages.
 */
public class JacksonMessageReader implements MessageReader {
    private final Reader       reader;
    private final ObjectMapper mapper;

    /**
     * Create reader with given object mapper.
     *
     * @param reader The reader to read from.
     * @param mapper The object mapper.
     */
    public JacksonMessageReader(Reader reader, ObjectMapper mapper) {
        this.reader = requireNonNull(reader, "reader == null");
        this.mapper = requireNonNull(mapper, "mapper == null").registerModule(new ProtoModule());
    }

    @Override
    public Message read(Descriptors.Descriptor descriptor) throws IOException {
        var type = ProtoMessage.getMessageClass(descriptor);
        return mapper.readValue(reader, type);
    }
}
