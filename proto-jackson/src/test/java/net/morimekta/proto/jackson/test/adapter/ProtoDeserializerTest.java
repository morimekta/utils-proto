package net.morimekta.proto.jackson.test.adapter;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.MessageOrBuilder;
import net.morimekta.proto.ProtoMessage;
import net.morimekta.proto.jackson.ProtoTypeRegistryOption;
import net.morimekta.proto.jackson.test.TestExt;
import net.morimekta.proto.jackson.test.TestProto;
import net.morimekta.proto.jackson.test.TestProto.ProtoCompact;
import net.morimekta.proto.jackson.test.TestProto.ProtoNormal;
import net.morimekta.proto.jackson.test.TestProto.ProtoValue;
import net.morimekta.proto.jackson.test.TestV2;
import net.morimekta.proto.utils.ProtoTypeRegistry;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.util.stream.Stream;

import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.proto.jackson.ProtoFeature.FAIL_ON_NULL_VALUE;
import static net.morimekta.proto.jackson.ProtoFeature.FAIL_ON_UNKNOWN_ENUM;
import static net.morimekta.proto.jackson.ProtoFeature.FAIL_ON_UNKNOWN_FIELD;
import static net.morimekta.proto.jackson.ProtoFeature.IGNORE_UNKNOWN_ANY_TYPE;
import static net.morimekta.proto.jackson.ProtoFeature.WRITE_COMPACT_MESSAGES;
import static net.morimekta.proto.jackson.ProtoFeature.WRITE_DURATION_AS_STRING;
import static net.morimekta.proto.jackson.ProtoFeature.WRITE_ENUM_AS_NUMBER;
import static net.morimekta.proto.jackson.ProtoFeature.WRITE_TIMESTAMP_AS_ISO;
import static net.morimekta.proto.jackson.ProtoFeature.disableFeatures;
import static net.morimekta.proto.jackson.ProtoFeature.enableFeatures;
import static net.morimekta.proto.jackson.test.ProtoModuleTest.wrap;
import static net.morimekta.proto.jackson.test.TestProto.ProtoValue.FIRST;
import static net.morimekta.proto.jackson.test.TestProto.ProtoValue.FOURTH;
import static net.morimekta.proto.jackson.test.TestProto.ProtoValue.OTHER;
import static net.morimekta.proto.jackson.test.TestProto.ProtoValue.SECOND;
import static net.morimekta.proto.jackson.test.TestProto.ProtoValue.THIRD;
import static net.morimekta.proto.utils.GoogleTypesUtil.makeProtoDuration;
import static net.morimekta.proto.utils.GoogleTypesUtil.makeProtoTimestamp;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ProtoDeserializerTest {
    public static Stream<Arguments> argsDeserialize_OK() {
        return Stream.of(
                // compact message
                arguments("[]", ProtoCompact.getDefaultInstance(), true, "{}"),
                arguments("[1]", ProtoCompact.newBuilder().setI(1).build(), true, "{\"i\":1}"),
                arguments("[1,null]", ProtoCompact.newBuilder().setI(1).build(), true, "{\"i\":1}"),
                arguments("[null,2]", ProtoCompact.newBuilder().setL(2L).build(), true, "{\"l\":2}"),
                arguments("[1,2]", ProtoCompact.newBuilder().setI(1).setL(2L).build(), true, "{\"l\":2,\"i\":1}"),
                arguments("{\"l\":2}", ProtoCompact.newBuilder().setL(2L).build(), true, null),
                // normal message
                arguments("{\"if_map\":{\"2\":2.5}}",
                          TestProto.ProtoFields.newBuilder().putIfMap(2, 2.5f).build(),
                          true,
                          null),
                arguments("{\"seMap\":{\"2\":\"OTHER\"}}",
                          TestProto.ProtoFields.newBuilder().putSeMap("2", OTHER).build(),
                          true,
                          "{\"se_map\":{\"2\":\"SECOND\"}}"),
                arguments("{\"fibs\":[\"SECOND\"]}",
                          TestProto.ProtoFields.newBuilder().addFibs(OTHER).build(),
                          true,
                          null),
                arguments("{\"compact\":[1,2,3]}",
                          TestProto.ProtoFields.newBuilder()
                                               .setCompact(ProtoCompact.newBuilder().setI(1).setL(2).build())
                                               .build(),
                          false,
                          "{\"compact\":{\"l\":2,\"i\":1}}"),
                // extension
                arguments("{\"test.ext.complex\":\"foo\"}",
                          TestV2.SimpleMessage.newBuilder().setExtension(TestExt.complex, "foo").build(),
                          true,
                          null),
                arguments("{\"1001\":\"foo\"}",
                          TestV2.SimpleMessage.newBuilder().setExtension(TestExt.complex, "foo").build(),
                          true,
                          "{\"test.ext.complex\":\"foo\"}"),
                // single enum value
                arguments("\"OTHER\"", SECOND, true, "\"SECOND\""),
                arguments("1", FIRST, true, "\"FIRST\""),
                // wrapped maps
                arguments("{\"ebMap\" : {\"3\" : \"\"}}",
                          wrap(wrapper -> wrapper.ebMap = mapOf(THIRD, ByteString.EMPTY)),
                          true,
                          "{\"ebMap\":{\"THIRD\":\"\"}}"));
    }

    @MethodSource("argsDeserialize_OK")
    @ParameterizedTest
    public void testDeserialize_OK(String origin, Object value, boolean strict, String serialized)
            throws JsonProcessingException {
        var mapper = new ObjectMapper();
        if (strict) {
            enableFeatures(mapper, FAIL_ON_NULL_VALUE, FAIL_ON_UNKNOWN_ENUM, FAIL_ON_UNKNOWN_FIELD);
        } else {
            disableFeatures(mapper, FAIL_ON_NULL_VALUE, FAIL_ON_UNKNOWN_ENUM, FAIL_ON_UNKNOWN_FIELD);
        }
        ProtoTypeRegistryOption.setRegistry(
                mapper,
                ProtoTypeRegistry.newBuilder()
                                 .register(TestExt.getDescriptor())
                                 .build());
        mapper.findAndRegisterModules();
        assertThat(mapper.readValue(origin, value.getClass()), is(value));
        if (serialized == null) {
            assertThat(mapper.writeValueAsString(value), is(origin));
        } else {
            assertThat(mapper.writeValueAsString(value), is(serialized));
            assertThat(mapper.readValue(serialized, value.getClass()), is(value));
        }
    }

    @Test
    public void testDeserialize_GoogleTypes() throws JsonProcessingException {
        var expectedMessage = TestProto.ProtoFields
                .newBuilder()
                .setTs(makeProtoTimestamp(1234567890, SECONDS))
                .setDur(makeProtoDuration(123, SECONDS))
                .build();
        var json = "{" +
                   "\"ts\":\"2009-02-13T23:31:30Z\"," +
                   "\"dur\":\"123s\"" +
                   "}";
        var mapper = new ObjectMapper();
        enableFeatures(mapper, WRITE_TIMESTAMP_AS_ISO, WRITE_DURATION_AS_STRING);
        mapper.findAndRegisterModules();
        assertThat(mapper.readValue(json, TestProto.ProtoFields.class), is(expectedMessage));
        assertThat(mapper.writeValueAsString(expectedMessage), is(json));
        assertThat(mapper.readValue("{\"ts\":1234567890}", TestProto.ProtoFields.class).getTs(),
                   is(makeProtoTimestamp(1234567890L, SECONDS)));
        assertThat(mapper.readValue("{\"ts\":1234567890.123}", TestProto.ProtoFields.class).getTs(),
                   is(makeProtoTimestamp(1234567890123L, MILLISECONDS)));
        assertThat(mapper.readValue("{\"dur\":123}", TestProto.ProtoFields.class).getDur(),
                   is(makeProtoDuration(123, SECONDS)));
        assertThat(mapper.readValue("{\"dur\":123.456}", TestProto.ProtoFields.class).getDur(),
                   is(makeProtoDuration(123456, MILLISECONDS)));

        try {
            fail("No exception: " + mapper.readValue("{\"ts\":\"foo\"}", TestProto.ProtoFields.class));
        } catch (JsonMappingException e) {
            assertThat(e.getMessage(),
                       is("Expected ISO date format, but found: \"foo\"\n at [Source: (String)\"{\"ts\":\"foo\"}\"; line: 1, column: 7]"));
        }
        try {
            fail("No exception: " + mapper.readValue("{\"ts\":false}", TestProto.ProtoFields.class));
        } catch (JsonMappingException e) {
            assertThat(e.getMessage(),
                       is("Unknown value for google.protobuf.Timestamp: VALUE_FALSE\n at [Source: (String)\"{\"ts\":false}\"; line: 1, column: 7]"));
        }
        try {
            fail("No exception: " + mapper.readValue("{\"dur\":\"foo\"}", TestProto.ProtoFields.class));
        } catch (JsonMappingException e) {
            assertThat(e.getMessage(),
                       is("Expected duration string, but found: \"foo\"\n at [Source: (String)\"{\"dur\":\"foo\"}\"; line: 1, column: 8]"));
        }
        try {
            fail("No exception: " + mapper.readValue("{\"dur\":false}", TestProto.ProtoFields.class));
        } catch (JsonMappingException e) {
            assertThat(e.getMessage(),
                       is("Unknown value for google.protobuf.Duration: VALUE_FALSE\n at [Source: (String)\"{\"dur\":false}\"; line: 1, column: 8]"));
        }
    }

    @Test
    public void testDeserialize_FailOnNull() throws JsonProcessingException {
        var mapper = new ObjectMapper();
        FAIL_ON_NULL_VALUE.enable(mapper);
        mapper.findAndRegisterModules();
        try {
            fail("no exception: " + mapper.readValue("{\"compact\":null,\"flt\":7}", TestProto.ProtoFields.class));
        } catch (IOException e) {
            assertThat(e.getMessage(),
                       is("Null value for field test.ProtoFields.compact\n at [Source: (String)\"{\"compact\":null,\"flt\":7}\"; line: 1, column: 12]"));
        }
        // this is not affected, as it never reaches the message deserializer.
        assertThat(mapper.readValue("null", ProtoCompact.class), is(nullValue()));
    }

    @Test
    public void testDeserialize_AllowNull() throws JsonProcessingException {
        var mapper = new ObjectMapper();
        FAIL_ON_NULL_VALUE.disable(mapper);
        mapper.findAndRegisterModules();

        assertThat(mapper.readValue("null", ProtoCompact.class), is(nullValue()));
        assertThat(mapper.readValue("{\"compact\":null}", TestProto.ProtoFields.class),
                   is(TestProto.ProtoFields.getDefaultInstance()));
    }

    @Test
    public void testDeserialize_Unknown() throws JsonProcessingException {
        var mapper = new ObjectMapper();
        FAIL_ON_UNKNOWN_FIELD.disable(WRITE_ENUM_AS_NUMBER.disable(mapper));
        FAIL_ON_UNKNOWN_ENUM.disable(WRITE_COMPACT_MESSAGES.disable(mapper));
        mapper.findAndRegisterModules();
        assertThat(mapper.readValue("{\"gorgon\":[4,6]}", TestProto.ProtoFields.class),
                   is(TestProto.ProtoFields.getDefaultInstance()));
        assertThat(mapper.readValue("{\"fib\":6}", TestProto.ProtoFields.class),
                   is(TestProto.ProtoFields.getDefaultInstance()));
        assertThat(mapper.readValue("{\"fib\":\"FOO\"}", TestProto.ProtoFields.class),
                   is(TestProto.ProtoFields.getDefaultInstance()));

        assertThat(mapper.readValue("{\"fib\":5}", TestProto.ProtoFields.class),
                   is(TestProto.ProtoFields.newBuilder().setFib(FOURTH).build()));
        assertThat(mapper.readValue("{\"fib\":\"FOURTH\"}", TestProto.ProtoFields.class),
                   is(TestProto.ProtoFields.newBuilder().setFib(FOURTH).build()));
    }

    public static Stream<Arguments> argsDeserialize_MappingFail() {
        return Stream.of(
                arguments("[1,2]",
                          ProtoNormal.class,
                          false,
                          "Array notation not allowed for test.ProtoNormal\n at [Source: (String)\"[1,2]\"; line: 1, column: 1]"),
                arguments("{}",
                          ProtoValue.class,
                          false,
                          "Invalid test.ProtoValue enum value token '{'\n at [Source: (String)\"{}\"; line: 1, column: 1]"),
                arguments("\"FOO\"",
                          ProtoValue.class,
                          true,
                          "Unknown test.ProtoValue enum name \"FOO\"\n at [Source: (String)\"\"FOO\"\"; line: 1, column: 1]"),
                arguments("7",
                          ProtoValue.class,
                          true,
                          "Unknown test.ProtoValue enum number 7\n at [Source: (String)\"7\"; line: 1, column: 1]"),
                arguments("{\"fib\":4.4}",
                          TestProto.ProtoFields.class,
                          false,
                          "Invalid type for enum value VALUE_NUMBER_FLOAT\n at [Source: (String)\"{\"fib\":4.4}\"; line: 1, column: 8]"),
                arguments("{\"fib\":\"FOO\"}",
                          TestProto.ProtoFields.class,
                          true,
                          "Unknown test.ProtoFields.fib value \"FOO\"\n at [Source: (String)\"{\"fib\":\"FOO\"}\"; line: 1, column: 8]"),
                arguments("{\"fib\":7}",
                          TestProto.ProtoFields.class,
                          true,
                          "Unknown test.ProtoFields.fib value 7\n at [Source: (String)\"{\"fib\":7}\"; line: 1, column: 8]"),
                arguments("{\"bools\":{}}",
                          TestProto.ProtoFields.class,
                          true,
                          "Expected '[' but got '{'\n at [Source: (String)\"{\"bools\":{}}\"; line: 1, column: 10]"),
                arguments("{\"any\":{\"@type\":\"foo\"}}}",
                          TestProto.ProtoFields.class,
                          false,
                          "Unknown type for unpacked any: foo\n at [Source: (String)\"{\"any\":{\"@type\":\"foo\"}}}\"; line: 1, column: 17]"),
                arguments("{\"1002\":{}}}",
                          TestV2.SimpleMessage.class,
                          true,
                          "Unknown field 1002 for test.v2.SimpleMessage\n at [Source: (String)\"{\"1002\":{}}}\"; line: 1, column: 2]"),
                arguments("{\"compact\":6}",
                          TestProto.ProtoFields.class,
                          true,
                          "Unknown start of object for test.ProtoCompact: VALUE_NUMBER_INT\n at [Source: (String)\"{\"compact\":6}\"; line: 1, column: 12]"),
                arguments("{\"compact\":[1,2,3]}",
                          TestProto.ProtoFields.class,
                          true,
                          "Values after last field for test.ProtoCompact\n at [Source: (String)\"{\"compact\":[1,2,3]}\"; line: 1, column: 17]"),
                arguments("{\"boolean\":[]}",
                          TestProto.ProtoFields.class,
                          true,
                          "Expected 'true' or 'false' but got '['\n at [Source: (String)\"{\"boolean\":[]}\"; line: 1, column: 12]"),
                arguments("{\"int\":\"int\"}",
                          TestProto.ProtoFields.class,
                          true,
                          "Invalid integer \"int\"\n at [Source: (String)\"{\"int\":\"int\"}\"; line: 1, column: 8]"),
                arguments("{\"int\":false}",
                          TestProto.ProtoFields.class,
                          true,
                          "Expected <integer> or \"<string>\" but got 'false'\n at [Source: (String)\"{\"int\":false}\"; line: 1, column: 8]"),
                arguments("{\"long\":\"long\"}",
                          TestProto.ProtoFields.class,
                          true,
                          "Invalid long integer \"long\"\n at [Source: (String)\"{\"long\":\"long\"}\"; line: 1, column: 9]"),
                arguments("{\"long\":false}",
                          TestProto.ProtoFields.class,
                          true,
                          "Expected <integer> or \"<string>\" but got 'false'\n at [Source: (String)\"{\"long\":false}\"; line: 1, column: 9]"),
                arguments("{\"flt\":\"flt\"}",
                          TestProto.ProtoFields.class,
                          true,
                          "Invalid float \"flt\"\n at [Source: (String)\"{\"flt\":\"flt\"}\"; line: 1, column: 8]"),
                arguments("{\"flt\":false}",
                          TestProto.ProtoFields.class,
                          true,
                          "Expected <real>, <integer> or \"<string>\" but got 'false'\n at [Source: (String)\"{\"flt\":false}\"; line: 1, column: 8]"),
                arguments("{\"dbl\":\"dbl\"}",
                          TestProto.ProtoFields.class,
                          true,
                          "Invalid double \"dbl\"\n at [Source: (String)\"{\"dbl\":\"dbl\"}\"; line: 1, column: 8]"),
                arguments("{\"dbl\":false}",
                          TestProto.ProtoFields.class,
                          true,
                          "Expected <real>, <integer> or \"<string>\" but got 'false'\n at [Source: (String)\"{\"dbl\":false}\"; line: 1, column: 8]"),
                arguments("{\"unknown\":[]}",
                          TestProto.ProtoFields.class,
                          true,
                          "Unknown field unknown for test.ProtoFields\n at [Source: (String)\"{\"unknown\":[]}\"; line: 1, column: 2]"));
    }

    @MethodSource("argsDeserialize_MappingFail")
    @ParameterizedTest
    public void testDeserialize_MappingFail(String serialized, Class<?> type, boolean strict, String message)
            throws JsonProcessingException {
        var mapper = new ObjectMapper();
        if (strict) {
            enableFeatures(mapper, FAIL_ON_UNKNOWN_FIELD, FAIL_ON_UNKNOWN_ENUM, FAIL_ON_NULL_VALUE);
        } else {
            disableFeatures(mapper, FAIL_ON_UNKNOWN_FIELD, FAIL_ON_UNKNOWN_ENUM, FAIL_ON_NULL_VALUE);
        }
        ProtoTypeRegistryOption.setRegistry(
                mapper,
                ProtoTypeRegistry.newBuilder()
                                 .register(TestExt.getDescriptor())
                                 .build());
        mapper.findAndRegisterModules();
        try {
            fail("No exception: " + new ProtoMessage((MessageOrBuilder) mapper.readValue(serialized, type)));
        } catch (JsonMappingException e) {
            assertThat(e.getMessage(), is(message));
        }
    }

    @Test
    public void testAny_IgnoreUnknownAnyType() throws JsonProcessingException {
        var mapper = new ObjectMapper();
        IGNORE_UNKNOWN_ANY_TYPE.enable(mapper);
        mapper.findAndRegisterModules();
        var out = mapper.readValue("{\"any\":{\"@type\":\"foo\"}}", TestProto.ProtoFields.class);
        assertThat(out,
                   is(TestProto.ProtoFields.newBuilder()
                                           .setAny(Any.newBuilder()
                                                      .setTypeUrl("foo")
                                                      .build())
                                           .build()));
    }
}
