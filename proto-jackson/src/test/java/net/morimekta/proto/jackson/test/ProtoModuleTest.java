package net.morimekta.proto.jackson.test;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import net.morimekta.proto.jackson.ProtoFeature;
import net.morimekta.proto.jackson.ProtoTypeRegistryOption;
import net.morimekta.proto.utils.ProtoTypeRegistry;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.Map;
import java.util.Objects;
import java.util.function.Consumer;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.proto.jackson.ProtoFeature.WRITE_ENUM_AS_NUMBER;
import static net.morimekta.proto.jackson.ProtoFeature.WRITE_FIELD_AS_NUMBER;
import static net.morimekta.proto.jackson.ProtoFeature.WRITE_UNPACKED_ANY;
import static net.morimekta.proto.jackson.ProtoFeature.disableFeatures;
import static net.morimekta.proto.jackson.ProtoFeature.enableFeatures;
import static net.morimekta.proto.jackson.ProtoStringFeature.ANY_TYPE_FIELD_NAME;
import static net.morimekta.proto.jackson.ProtoStringFeature.ANY_TYPE_PREFIX;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ProtoModuleTest {
    public static Stream<Arguments> argsDeserialize_OK() {
        return Stream.of(
                arguments(
                        TestProto.ProtoFields.newBuilder()
                                             .setBoolean(true)
                                             .setBin(ByteString.copyFromUtf8("foo"))
                                             .setFib(TestProto.ProtoValue.FIFTH)
                                             .setCompact(TestProto.ProtoCompact.newBuilder()
                                                                               .setL(5)
                                                                               .build())
                                             .build(),
                        ProtoFeature.WRITE_COMPACT_MESSAGES.enable(new ObjectMapper()),
                        "{\n" +
                        "  \"boolean\" : true,\n" +
                        "  \"bin\" : \"Zm9v\",\n" +
                        "  \"fib\" : \"FIFTH\",\n" +
                        "  \"compact\" : [ null, 5 ]\n" +
                        "}"),
                arguments(
                        TestProto.ProtoFields.newBuilder()
                                             .setBin(ByteString.copyFromUtf8("foo"))
                                             .setFib(TestProto.ProtoValue.FIFTH)
                                             .build(),
                        enableFeatures(new ObjectMapper(), WRITE_ENUM_AS_NUMBER, WRITE_FIELD_AS_NUMBER),
                        "{\n" +
                        "  \"21\" : \"Zm9v\",\n" +
                        "  \"22\" : 8\n" +
                        "}"),
                arguments(
                        TestProto.ProtoFields.newBuilder()
                                             .setFlt(2.4f)
                                             .setDbl(4.8d)
                                             .addAllIntegers(listOf(1, 2, 3))
                                             .build(),
                        new ObjectMapper(),
                        "{\n" +
                        "  \"flt\" : 2.4,\n" +
                        "  \"dbl\" : 4.8,\n" +
                        "  \"integers\" : [ 1, 2, 3 ]\n" +
                        "}"),
                arguments(
                        TestProto.ProtoFields.newBuilder()
                                             .putAllLiMap(mapOf(42L, 55))
                                             .putAllBsMap(mapOf(false, "false", true, "true"))
                                             .build(),
                        new ObjectMapper(),
                        "{\n" +
                        "  \"bs_map\" : {\n" +
                        "    \"false\" : \"false\",\n" +
                        "    \"true\" : \"true\"\n" +
                        "  },\n" +
                        "  \"li_map\" : {\n" +
                        "    \"42\" : 55\n" +
                        "  }\n" +
                        "}"),
                arguments(
                        TestProto.ProtoFields.newBuilder()
                                             .setAny(Any.pack(TestProto.ProtoNormal.newBuilder().setI(42).build()))
                                             .build(),
                        mapper(mapper -> {
                            ANY_TYPE_FIELD_NAME.set(mapper, "@");
                            ANY_TYPE_PREFIX.set(mapper, "/");
                            enableFeatures(mapper, WRITE_UNPACKED_ANY);
                        }),
                        "{\n" +
                        "  \"any\" : {\n" +
                        "    \"@\" : \"/test.ProtoNormal\",\n" +
                        "    \"i\" : 42\n" +
                        "  }\n" +
                        "}"),
                arguments(
                        TestProto.ProtoFields.newBuilder()
                                             .setAny(Any.pack(TestProto.ProtoNormal.newBuilder().setI(42).build()))
                                             .build(),
                        mapper(mapper -> enableFeatures(mapper, WRITE_UNPACKED_ANY)),
                        "{\n" +
                        "  \"any\" : {\n" +
                        "    \"@type\" : \"type.googleapis.com/test.ProtoNormal\",\n" +
                        "    \"i\" : 42\n" +
                        "  }\n" +
                        "}"),
                arguments(
                        TestProto.ProtoFields.newBuilder()
                                             .setAny(Any.pack(TestProto.ProtoNormal.newBuilder().setI(42).build()))
                                             .build(),
                        mapper(mapper -> disableFeatures(mapper, WRITE_UNPACKED_ANY)),
                        "{\n" +
                        "  \"any\" : {\n" +
                        "    \"type_url\" : \"type.googleapis.com/test.ProtoNormal\",\n" +
                        "    \"value\" : \"IFQ\"\n" +
                        "  }\n" +
                        "}"),
                arguments(wrap(wrapper -> wrapper.beMap = mapOf(ByteString.copyFromUtf8("→ŋøđ©"),
                                                                TestProto.ProtoValue.SECOND)),
                          new ObjectMapper(),
                          "{\n" +
                          "  \"beMap\" : {\n" +
                          "    \"4oaSxYvDuMSRwqk\" : \"SECOND\"\n" +
                          "  }\n" +
                          "}"),
                arguments(wrap(wrapper -> wrapper.ebMap = mapOf(TestProto.ProtoValue.THIRD,
                                                                ByteString.copyFromUtf8("→ŋøđ©"))),
                          new ObjectMapper(),
                          "{\n" +
                          "  \"ebMap\" : {\n" +
                          "    \"THIRD\" : \"4oaSxYvDuMSRwqk\"\n" +
                          "  }\n" +
                          "}"),
                arguments(wrap(wrapper -> {
                              wrapper.ebMap = mapOf(TestProto.ProtoValue.THIRD, ByteString.copyFromUtf8("→ŋøđ©"));
                              wrapper.value = TestProto.ProtoValue.THIRD;
                          }),
                          enableFeatures(new ObjectMapper(), WRITE_ENUM_AS_NUMBER),
                          "{\n" +
                          "  \"value\" : 3,\n" +
                          "  \"ebMap\" : {\n" +
                          "    \"3\" : \"4oaSxYvDuMSRwqk\"\n" +
                          "  }\n" +
                          "}"),
                arguments(wrap(wrapper -> wrapper.bytes = ByteString.copyFromUtf8("→ŋøđ©")),
                          new ObjectMapper(),
                          "{\n" +
                          "  \"bytes\" : \"4oaSxYvDuMSRwqk\"\n" +
                          "}"),
                arguments(TestProto.ProtoFields.getDefaultInstance(),
                          new ObjectMapper(),
                          "{ }")
                        );
    }

    @JsonInclude(JsonInclude.Include.NON_NULL)
    public static class Wrapper {
        public ByteString                            bytes;
        public TestProto.ProtoValue                  value;
        public Map<ByteString, TestProto.ProtoValue> beMap;
        public Map<TestProto.ProtoValue, ByteString> ebMap;

        @Override
        public boolean equals(Object o) {
            if (this == o) {
                return true;
            }
            if (o == null || getClass() != o.getClass()) {
                return false;
            }
            Wrapper wrapper = (Wrapper) o;
            return Objects.equals(bytes, wrapper.bytes) &&
                   Objects.equals(beMap, wrapper.beMap) &&
                   Objects.equals(ebMap, wrapper.ebMap) &&
                   Objects.equals(value, wrapper.value);
        }

        @Override
        public int hashCode() {
            return Objects.hash(bytes, beMap, ebMap, value);
        }

        @Override
        public String toString() {
            return "Wrapper{" +
                   "bytes=" + bytes +
                   ", value=" + value +
                   ", beMap=" + beMap +
                   ", ebMap=" + ebMap +
                   '}';
        }
    }

    public static Wrapper wrap(Consumer<Wrapper> f) {
        Wrapper wrapper = new Wrapper();
        f.accept(wrapper);
        return wrapper;
    }

    public static ObjectMapper mapper(Consumer<ObjectMapper> f) {
        ObjectMapper mapper = new ObjectMapper();
        f.accept(mapper);
        return mapper;
    }

    @MethodSource("argsDeserialize_OK")
    @ParameterizedTest
    public void testDeserialize_OK(Object message,
                                   ObjectMapper mapper,
                                   String expected) throws JsonProcessingException {
        mapper.findAndRegisterModules();
        ProtoTypeRegistryOption.setRegistry(mapper, ProtoTypeRegistry.newBuilder()
                                                                     .register(TestProto.getDescriptor())
                                                                     .build());
        var serialized = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(message);
        var parsed = mapper.readValue(serialized, message.getClass());
        assertThat(serialized, is(expected));
        assertThat(parsed, is(message));
    }
}
