package net.morimekta.proto.jackson.test.sio;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.google.protobuf.Any;
import com.google.protobuf.util.JsonFormat;
import net.morimekta.proto.jackson.ProtoTypeRegistryOption;
import net.morimekta.proto.jackson.sio.JacksonMessageReader;
import net.morimekta.proto.jackson.sio.JacksonMessageWriter;
import net.morimekta.proto.jackson.test.TestProto;
import net.morimekta.proto.jackson.test.TestProto.ProtoCompact;
import net.morimekta.proto.jackson.test.TestProto.ProtoFields;
import net.morimekta.proto.utils.GoogleTypesUtil;
import net.morimekta.proto.utils.ProtoTypeRegistry;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.List;
import java.util.concurrent.TimeUnit;

import static net.morimekta.proto.testing.ProtoMatchers.equalToMessage;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class JacksonMessageRWTest {
    @Test
    public void testJson() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        StringReader reader = new StringReader(
                "{\n" +
                "  \"boolean\": true,\n" +
                "  \"compacts\": [\n" +
                "    {\n" +
                "      \"i\": 5,\n" +
                "      \"l\": 7\n" +
                "    }\n" +
                "  ]\n" +
                "}\n" +
                "");
        ProtoFields fields = (ProtoFields) new JacksonMessageReader(reader, mapper).read(ProtoFields.getDescriptor());
        assertThat(fields, is(notNullValue()));
        assertThat(fields.getBoolean(), is(true));
        assertThat(fields.getCompactsList(), is(List.of(ProtoCompact.newBuilder().setI(5).setL(7).build())));

        StringWriter out = new StringWriter();
        new JacksonMessageWriter(out, mapper, false).write(fields);
        assertThat(out.getBuffer().toString(),
                   is("{\"boolean\":true,\"compacts\":[{\"l\":7,\"i\":5}]}"));
        out.getBuffer().delete(0, out.getBuffer().length());
        new JacksonMessageWriter(out, mapper, true).write(fields);
        assertThat(out.getBuffer().toString(),
                   is("{\n" +
                      "  \"boolean\" : true,\n" +
                      "  \"compacts\" : [ {\n" +
                      "    \"l\" : 7,\n" +
                      "    \"i\" : 5\n" +
                      "  } ]\n" +
                      "}\n" +
                      ""));
    }

    @Test
    public void testYaml() throws IOException {
        ObjectMapper mapper = new ObjectMapper(new YAMLFactory());
        StringReader reader = new StringReader(
                "---\n" +
                "boolean: true\n" +
                "compact:\n" +
                "  i: 7\n" +
                "  l: 9\n" +
                "compacts:\n" +
                "- i: 5\n" +
                "  l: 7\n" +
                "");
        ProtoFields fields = (ProtoFields) new JacksonMessageReader(reader, mapper).read(ProtoFields.getDescriptor());
        assertThat(fields, is(notNullValue()));
        assertThat(fields.getBoolean(), is(true));
        assertThat(fields.getCompact(), is(ProtoCompact.newBuilder().setI(7).setL(9).build()));
        assertThat(fields.getCompactsList(), is(List.of(ProtoCompact.newBuilder().setI(5).setL(7).build())));

        StringWriter out = new StringWriter();
        new JacksonMessageWriter(out, mapper, false).write(fields);
        assertThat(out.getBuffer().toString(),
                   is("---\n" +
                      "boolean: true\n" +
                      "compact:\n" +
                      "  l: 9\n" +
                      "  i: 7\n" +
                      "compacts:\n" +
                      "- l: 7\n" +
                      "  i: 5\n" +
                      ""));
    }

    @Test
    public void testJsonFormatCompatibility() throws IOException {
        var fields = ProtoFields.newBuilder()
                                .setAny(Any.pack(TestProto.ProtoNormal.newBuilder().setI(42).build()))
                                .setDur(GoogleTypesUtil.makeProtoDuration(13579, TimeUnit.MILLISECONDS))
                                .setTs(GoogleTypesUtil.makeProtoTimestamp(1234567890, TimeUnit.SECONDS))
                                .putBsMap(true, "YAY")
                                .setDbl(43.21d)
                                .addCompacts(ProtoCompact.newBuilder().setL(42L).build())
                                .build();

        var registry = JsonFormat.TypeRegistry.newBuilder().add(TestProto.ProtoNormal.getDescriptor()).build();
        var typeRegistry = ProtoTypeRegistry.newBuilder().register(TestProto.ProtoNormal.getDescriptor()).build();

        ObjectMapper mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        ProtoTypeRegistryOption.setRegistry(mapper, typeRegistry);

        var jfJson = JsonFormat.printer()
                               .preservingProtoFieldNames()
                               .usingTypeRegistry(registry)
                               .omittingInsignificantWhitespace()
                               .print(fields);
        var upJson = mapper.writeValueAsString(fields);

        assertThat(mapper.readValue(jfJson, ProtoFields.class), equalToMessage(fields));

        var builder = ProtoFields.newBuilder();
        JsonFormat.parser().usingTypeRegistry(registry).merge(new StringReader(upJson), builder);
        assertThat(builder.build(), is(fields));

        // Because of nits in handling some numbers, this will always be a little different.
        // at least it parses
        // assertThat(jfJson, is(upJson));
    }
}
