module net.morimekta.proto.jackson.test {
    requires com.fasterxml.jackson.annotation;
    requires com.fasterxml.jackson.databind;
    requires com.fasterxml.jackson.dataformat.yaml;
    requires com.google.gson;
    requires com.google.protobuf.util;
    requires com.google.protobuf;
    requires net.morimekta.collect;
    requires net.morimekta.proto.jackson;
    requires net.morimekta.proto.testing;
    requires net.morimekta.proto;
    requires org.hamcrest;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
    requires org.junit.jupiter.params;

    exports net.morimekta.proto.jackson.test;
    exports net.morimekta.proto.jackson.test.adapter;
    exports net.morimekta.proto.jackson.test.sio;
}