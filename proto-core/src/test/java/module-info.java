module net.morimekta.proto.test {
    requires com.google.protobuf;
    requires net.morimekta.collect;
    requires net.morimekta.proto;
    requires net.morimekta.strings;
    requires org.hamcrest;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
    requires org.junit.jupiter.params;
    requires org.mockito.junit.jupiter;

    exports net.morimekta.proto.test;
    exports net.morimekta.proto.test.sio;
    exports net.morimekta.proto.test.utils;
}