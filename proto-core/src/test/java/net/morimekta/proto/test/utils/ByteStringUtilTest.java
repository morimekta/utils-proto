package net.morimekta.proto.test.utils;

import com.google.protobuf.ByteString;
import org.junit.jupiter.api.Test;

import static com.google.protobuf.ByteString.copyFromUtf8;
import static net.morimekta.proto.utils.ByteStringUtil.fromBase64;
import static net.morimekta.proto.utils.ByteStringUtil.fromHexString;
import static net.morimekta.proto.utils.ByteStringUtil.toBase64;
import static net.morimekta.proto.utils.ByteStringUtil.toHexString;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ByteStringUtilTest {
    @Test
    public void testBase64() {
        assertThat(toBase64(null), is(nullValue()));
        assertThat(toBase64(ByteString.EMPTY), is(""));
        String data = toBase64(copyFromUtf8("foo"));
        assertThat(data, is("Zm9v"));
        assertThat(fromBase64(data), is(copyFromUtf8("foo")));
        assertThat(fromBase64(null), is(nullValue()));
        assertThat(fromBase64(""), is(ByteString.EMPTY));
    }

    @Test
    public void testHexString() {
        assertThat(toHexString(null), is(nullValue()));
        assertThat(toHexString(ByteString.EMPTY), is(""));
        String data = toHexString(copyFromUtf8("foo"));
        assertThat(data, is("666f6f"));
        assertThat(fromHexString(data), is(copyFromUtf8("foo")));
        assertThat(fromHexString(null), is(nullValue()));
        assertThat(fromHexString(""), is(ByteString.EMPTY));
    }
}
