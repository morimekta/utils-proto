package net.morimekta.proto.test.utils;

import com.google.protobuf.Message;
import net.morimekta.proto.test.TestFields;
import org.junit.jupiter.api.Test;

import static net.morimekta.proto.utils.ReflectionUtil.getSingleton;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ReflectionUtilTest {
    @Test
    public void testSingletonGet() {
        assertThat(getSingleton(TestFields.DefaultMessage.class, Message.class, "getDefaultInstance"),
                   is(instanceOf(TestFields.DefaultMessage.class)));

        try {
            getSingleton(ProtectedType.class, String.class, "privateSingleton");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Inaccessible singleton ProtectedType.privateSingleton()"));
        }
        try {
            getSingleton(ProtectedType.class, String.class, "nullSingleton");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Null singleton value: ProtectedType.nullSingleton()"));
        }
        try {
            getSingleton(ProtectedType.class, String.class, "notSingleton");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Non-static singleton getter: ProtectedType.notSingleton()"));
        }
        try {
            getSingleton(ProtectedType.class, String.class, "CONST");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No such singleton getter: ProtectedType.CONST()"));
        }
        try {
            getSingleton(ProtectedType.class, Integer.class, "publicSingleton");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is(
                    "Invalid singleton value: String ProtectedType.publicSingleton() " +
                    "not assignable to java.lang.Integer"));
        }
    }
}
