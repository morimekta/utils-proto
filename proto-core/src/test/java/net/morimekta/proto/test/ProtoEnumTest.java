package net.morimekta.proto.test;

import net.morimekta.proto.ProtoEnum;
import net.morimekta.proto.test.TestFields.Fibonacci;
import net.morimekta.proto.test.TestOuter.TestEnum;
import net.morimekta.proto.test.TestOuter.TestEnum2;
import net.morimekta.proto.test.TestOuter.TestEnum3;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static net.morimekta.proto.ProtoEnum.getEnumDescriptor;
import static net.morimekta.proto.ProtoEnum.getEnumDescriptorUnchecked;
import static net.morimekta.proto.ProtoEnum.isProtoEnumClass;
import static net.morimekta.proto.ProtoEnum.requireProtoEnum;
import static net.morimekta.proto.ProtoEnum.requireProtoEnumClass;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ProtoEnumTest {
    @Test
    public void testV2Enum() {
        var pe1 = getEnumDescriptor(TestEnum.class);
        assertThat(pe1.getTypeName(), is("test.v2.TestEnum"));
        assertThat(pe1.getEnumClass(), is(TestEnum.class));
        assertThat(pe1.getProtoDescriptor(), is(TestEnum.getDescriptor()));
        assertThat(pe1.getDefaultValue(), is(Optional.empty()));

        var pe2 = getEnumDescriptor(TestEnum2.class);
        assertThat(pe2.getTypeName(), is("test.v2.TestEnum2"));
        assertThat(pe2.getEnumClass(), is(TestEnum2.class));
        assertThat(pe2.getProtoDescriptor(), is(TestEnum2.getDescriptor()));
        assertThat(pe2.getDefaultValue(), is(Optional.empty()));

        assertThat(pe2, is(not(pe1)));
        assertThat(pe2, is(pe2));
        assertThat(pe1, is(not("")));

        var pe2copy = new ProtoEnum<>(pe2.getEnumClass(), pe2.getProtoDescriptor()) {

        };
        assertThat(pe2, is(pe2copy));

        assertThat(pe2.toString(), is("test.v2.TestEnum2{other=2}"));
    }

    @Test
    public void testProtoEnumType() {
        assertThat(requireProtoEnum(TestEnum.THIRD), is(TestEnum.THIRD));
        try {
            requireProtoEnum(TestEnum.THIRD.getValueDescriptor());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a proto enum THIRD"));
        }
        try {
            requireProtoEnum(OtherEnum.BAZ);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a proto enum BAZ"));
        }
        try {
            requireProtoEnum(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("instance == null"));
        }
        assertThat(requireProtoEnumClass(Fibonacci.class), is(Fibonacci.class));
        try {
            requireProtoEnumClass(TestFields.DefaultMessage.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a proto enum type: net.morimekta.proto.test.TestFields.DefaultMessage"));
        }
        try {
            requireProtoEnumClass(OtherEnum.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a proto enum type: net.morimekta.proto.test.ProtoEnumTest.OtherEnum"));
        }
        try {
            requireProtoEnumClass(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("type == null"));
        }
        assertThat(isProtoEnumClass(Fibonacci.class), is(true));
        assertThat(isProtoEnumClass(OtherEnum.class), is(false));
        assertThat(isProtoEnumClass(null), is(false));
    }

    enum OtherEnum {
        BAZ
    }

    @Test
    public void testEnumDescriptor() {
        var te3 = getEnumDescriptor(TestEnum3.ALTERNATIVE);
        assertThat(te3, is(getEnumDescriptor(TestEnum3.getDescriptor())));
        try {
            getEnumDescriptor((Class<TestEnum2>) (Class) TestOuter.TestMessage.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a proto enum type: net.morimekta.proto.test.TestOuter.TestMessage"));
        }
    }

    @Test
    public void testEnumDescriptorUnchecked() {
        var te3 = getEnumDescriptor(TestEnum3.ALTERNATIVE);
        assertThat(te3, is(getEnumDescriptor(TestEnum3.getDescriptor())));
        try {
            getEnumDescriptorUnchecked(TestOuter.TestMessage.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a proto enum type: net.morimekta.proto.test.TestOuter.TestMessage"));
        }
    }

    @Test
    public void testV3Enum() {
        var fib = getEnumDescriptor(Fibonacci.class);
        assertThat(fib.getTypeName(), is("test.Fibonacci"));
        assertThat(fib.getEnumClass(), is(Fibonacci.class));
        assertThat(fib.getProtoDescriptor(), is(Fibonacci.getDescriptor()));
        assertThat(fib.getDefaultValue(), is(Optional.of(Fibonacci.NONE)));
        assertThat(fib.findByName(null), is(nullValue()));
        assertThat(fib.findByName("FIRST"), is(Fibonacci.FIRST));
        assertThat(fib.findByName("SECOND"), is(Fibonacci.SECOND));
        assertThat(fib.findByName("OTHER"), is(Fibonacci.SECOND));
        assertThat(fib.findByName("foo"), is(nullValue()));
        assertThat(fib.findByNumber(null), is(nullValue()));
        assertThat(fib.findByNumber(0), is(Fibonacci.NONE));
        assertThat(fib.findByNumber(2), is(Fibonacci.SECOND));
        assertThat(fib.findByNumber(7), is(nullValue()));
        assertThat(fib.findByValue(null), is(nullValue()));
        assertThat(fib.findByValue(Fibonacci.FOURTH.getValueDescriptor()), is(Fibonacci.FOURTH));
        assertThat(fib.findByValue(TestEnum.FIRST.getValueDescriptor()), is(nullValue()));

        assertThat(fib.valueForNumber(2), is(Fibonacci.SECOND));
        assertThat(fib.valueForName("SECOND"), is(Fibonacci.SECOND));
        assertThat(fib.valueFor(Fibonacci.SECOND.getValueDescriptor()), is(Fibonacci.SECOND));


        try {
            fib.valueForNumber(7);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No test.Fibonacci value for number 7"));
        }
        try {
            fib.valueForName(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("name == null"));
        }
        try {
            fib.valueForName("FOO");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No test.Fibonacci value for name 'FOO'"));
        }
        try {
            fib.valueFor(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("value == null"));
        }
        try {
            fib.valueFor(TestEnum.NO_VALUE.getValueDescriptor());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No test.Fibonacci value for test.v2.TestEnum.NO_VALUE"));
        }
    }
}
