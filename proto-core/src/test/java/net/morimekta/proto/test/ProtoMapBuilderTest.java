package net.morimekta.proto.test;

import net.morimekta.proto.ProtoMapBuilder;
import org.junit.jupiter.api.Test;

import java.util.AbstractMap;
import java.util.NoSuchElementException;

import static java.util.Map.entry;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ProtoMapBuilderTest {
    @Test
    public void testMap() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .putAllStringMap(mapOf(1, "foo", 3, "bar"));
        var map = new ProtoMapBuilder<Integer, String>(msg, msg.getDescriptorForType().findFieldByName("string_map"));
        assertThat(map.size(), is(2));
        assertThat(map, is(mapOf(1, "foo", 3, "bar")));
        assertThat(map.isEmpty(), is(false));
        assertThat(map.containsKey(2), is(false));
        assertThat(map.containsValue("foo"), is(true));
        assertThat(map.containsValue("baz"), is(false));
        assertThat(map.get(1), is("foo"));
        assertThat(map.get(2), is(nullValue()));
        assertThat(map.keySet(), is(setOf(1, 3)));
        assertThat(map.values(), is(listOf("foo", "bar")));
        assertThat(map.entrySet().size(), is(map.size()));
        assertThat(map.entrySet(), is(setOf(
                new AbstractMap.SimpleImmutableEntry<>(1, "foo"),
                new AbstractMap.SimpleImmutableEntry<>(3, "bar"))));

        assertThat(map.hashCode(), is(not(0)));
        assertThat(map.keySet().hashCode(), is(map.hashCode()));
        assertThat(map.values().hashCode(), is(map.hashCode()));
        assertThat(map.entrySet().hashCode(), is(map.hashCode()));

        assertThat(map.toString(), is("{1: \"foo\", 3: \"bar\"}"));
        assertThat(map.keySet().toString(), is("[1, 3]"));
        assertThat(map.values().toString(), is("[foo, bar]"));
        assertThat(map.entrySet().toString(), is("[1=foo, 3=bar]"));

        assertThat(map, is(mapOf(1, "foo", 3, "bar")));
        assertThat(map, is(not(mapOf(1, "foo", 3, "bar", 5, "baz"))));
        assertThat(map, is(not(mapOf(1, "foo", 5, "bar"))));
        assertThat(map, is(not(mapOf(1, "foo", 3, "baz"))));

        var map2 = new ProtoMapBuilder<>(msg, msg.getDescriptorForType().findFieldByName("string_map"));
        var map3 = new ProtoMapBuilder<>(msg, msg.getDescriptorForType().findFieldByName("sc_map"));

        assertThat(map, is(map));
        assertThat(map, is(map2));
        assertThat(map, is(not(map3)));
        assertThat(map, is(not("foo")));

        assertThat(map3.isEmpty(), is(true));

        // sub-collections.
        var ks = map.keySet();
        assertThat(ks, is(ks));
        assertThat(ks, is(map.keySet()));
        assertThat(ks, is(setOf(1, 3)));
        assertThat(ks, is(not("foo")));
        assertThat(ks, is(not(setOf(1))));
        assertThat(ks, is(not(setOf(1, 5))));

        var val = map.values();
        assertThat(val, is(val));
        assertThat(val, is(map.values()));
        assertThat(val, is(listOf("foo", "bar")));
        assertThat(val, is(not("foo")));
        assertThat(val, is(not(listOf("foo"))));
        assertThat(val, is(not(listOf("baz", "foo"))));

        var es = map.entrySet();
        assertThat(es, is(es));
        assertThat(es, is(setOf(entry(1, "foo"), entry(3, "bar"))));
        assertThat(es, is(not("foo")));
        assertThat(es, is(not(setOf(entry(1, "foo")))));
        assertThat(es, is(not(setOf(entry(1, "foo"), entry(3, "baz")))));
    }

    @Test
    public void testMapWrites() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .putAllStringMap(mapOf(1, "foo", 3, "bar"));
        var map = new ProtoMapBuilder<Integer, String>(msg, msg.getDescriptorForType().findFieldByName("string_map"));

        assertThat(map.entrySet(), is(setOf(
                new AbstractMap.SimpleImmutableEntry<>(1, "foo"),
                new AbstractMap.SimpleImmutableEntry<>(3, "bar"))));

        map.put(2, "baz");

        assertThat(map.entrySet(), is(setOf(
                new AbstractMap.SimpleImmutableEntry<>(1, "foo"),
                new AbstractMap.SimpleImmutableEntry<>(3, "bar"),
                new AbstractMap.SimpleImmutableEntry<>(2, "baz"))));

        assertThat(map.remove(1), is("foo"));

        assertThat(map.entrySet(), is(setOf(
                new AbstractMap.SimpleImmutableEntry<>(3, "bar"),
                new AbstractMap.SimpleImmutableEntry<>(2, "baz"))));

        map.putAll(mapOf(5, "ed", 2, "bier"));

        assertThat(map.entrySet(), is(setOf(
                new AbstractMap.SimpleImmutableEntry<>(3, "bar"),
                new AbstractMap.SimpleImmutableEntry<>(2, "bier"),
                new AbstractMap.SimpleImmutableEntry<>(5, "ed"))));

        map.clear();

        assertThat(map.entrySet(), is(setOf()));
    }

    // TODO: Test enum values.

    @Test
    public void testMapFails() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .putAllStringMap(mapOf(1, "foo", 3, "bar"));
        try {
            new ProtoMapBuilder<>(msg, msg.getDescriptorForType().findFieldByName("strings"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a map field: test.DefaultMessage.strings"));
        }
    }

    @Test
    public void testKeySetIterators() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .putAllStringMap(mapOf(1, "foo", 3, "bar"));
        var map = new ProtoMapBuilder<Integer, String>(msg, msg.getDescriptorForType().findFieldByName("string_map"));

        var keys = map.keySet().iterator();
        assertThat(keys.next(), is(1));
        keys.remove();
        try {
            keys.remove();
            fail("no exception");
        } catch (IllegalStateException e) {
            assertThat(e.getMessage(), is("No current element"));
        }
        assertThat(keys.next(), is(3));
        try {
            keys.next();
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("1 >= 1"));
        }
    }

    @Test
    public void testValuesIterator() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .putAllStringMap(mapOf(1, "foo", 3, "bar"));
        var map = new ProtoMapBuilder<Integer, String>(msg, msg.getDescriptorForType().findFieldByName("string_map"));

        var values = map.values().iterator();
        assertThat(values.next(), is("foo"));
        values.remove();
        try {
            values.remove();
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("No current element"));
        }
        assertThat(values.next(), is("bar"));
        try {
            values.next();
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("1 >= 1"));
        }
    }

    @Test
    public void testEntrySetIterator() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .putAllStringMap(mapOf(1, "foo", 3, "bar"));
        var map = new ProtoMapBuilder<Integer, String>(msg, msg.getDescriptorForType().findFieldByName("string_map"));

        var entries = map.entrySet().iterator();
        assertThat(entries.next(), is(new AbstractMap.SimpleImmutableEntry<>(1, "foo")));
        entries.remove();
        try {
            entries.remove();
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("No current element"));
        }
        var last = entries.next();
        assertThat(last, is(new AbstractMap.SimpleEntry<>(3, "bar")));
        last.setValue("biera");

        try {
            entries.next();
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("1 >= 1"));
        }

        assertThat(map.get(3), is("biera"));
    }
}
