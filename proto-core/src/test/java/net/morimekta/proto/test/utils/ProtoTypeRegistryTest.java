package net.morimekta.proto.test.utils;

import net.morimekta.proto.test.TestFields;
import net.morimekta.proto.test.TestFiles;
import net.morimekta.proto.test.TestOuter;
import net.morimekta.proto.utils.ProtoTypeRegistry;
import org.junit.jupiter.api.Test;

import static net.morimekta.proto.utils.ProtoTypeRegistry.getTypeNameFromTypeUrl;
import static net.morimekta.proto.utils.ProtoTypeRegistry.getTypeUrl;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoTypeRegistryTest {
    @Test
    public void testRegister() {
        var registry = ProtoTypeRegistry
                .newBuilder()
                .register(TestOuter.getDescriptor())
                .register(TestFiles.getDescriptor())
                .register(TestFiles.getDescriptor())
                .register(TestOuter.TestMessage.getDescriptor().findFieldByName("string"))
                .build();
        assertThat(registry.messageTypeByName("test.v2.TestMessage"), is(TestOuter.TestMessage.getDescriptor()));
        assertThat(registry.messageTypeByName("test.v2.TestMessage3.TestMessage2"),
                   is(TestOuter.TestMessage3.TestMessage2.getDescriptor()));
        assertThat(registry.messageTypeByTypeUrl("type.googleapis.com/test.v2.TestMessage3.TestMessage2"),
                   is(TestOuter.TestMessage3.TestMessage2.getDescriptor()));
        assertThat(registry.messageTypeByName("test.v2.Test"), is(nullValue()));
        assertThat(registry.messageTypeByTypeUrl("type.googleapis.com/test.v2.Test"), is(nullValue()));

        assertThat(registry.enumTypeByName("test.v2.TestEnum"), is(TestOuter.TestEnum.getDescriptor()));
        assertThat(registry.enumTypeByName("test.v2.Test"), is(nullValue()));

        assertThat(registry.enumValueByQualifiedIdentifier("test.v2.TestEnum.fourth"),
                   is(TestOuter.TestEnum.fourth.getValueDescriptor()));
        assertThat(registry.enumValueByQualifiedIdentifier("test.v2.TestEnum.eigth"), is(nullValue()));

        assertThat(registry.extensionByScopeAndName(TestOuter.TestMessage3.getDescriptor(), "test.files.extended"),
                   is(TestFiles.extended.getDescriptor()));
        assertThat(registry.extensionByScopeAndName(TestOuter.TestMessage3.getDescriptor(), "extended"),
                   is(nullValue()));
        assertThat(registry.extensionByScopeAndName(TestOuter.TestMessage.getDescriptor(), "test.files.extended"),
                   is(nullValue()));
        assertThat(registry.extensionByScopeAndNumber(TestOuter.TestMessage3.getDescriptor(), 1001),
                   is(TestFiles.extended.getDescriptor()));
        assertThat(registry.extensionByScopeAndNumber(TestOuter.TestMessage3.getDescriptor(), 1002), is(nullValue()));
        assertThat(registry.extensionByScopeAndNumber(TestOuter.TestMessage.getDescriptor(), 1001), is(nullValue()));
    }

    @Test
    public void testUtils() {
        assertThat(getTypeUrl(TestFields.DefaultMessage.getDescriptor()),
                   is("type.googleapis.com/test.DefaultMessage"));
        assertThat(getTypeUrl("", TestFields.DefaultMessage.getDescriptor()),
                   is("test.DefaultMessage"));
        assertThat(getTypeNameFromTypeUrl("test.DefaultMessage"),
                   is("test.DefaultMessage"));
        assertThat(getTypeNameFromTypeUrl("type.googleapis.com/test.DefaultMessage"),
                   is("test.DefaultMessage"));
    }
}
