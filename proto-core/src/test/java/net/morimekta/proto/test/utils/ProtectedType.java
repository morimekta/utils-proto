package net.morimekta.proto.test.utils;

public class ProtectedType {
    public final String CONST = "CONST";

    public static String publicSingleton() {
        return "PUBLIC";
    }

    private static String privateSingleton() {
        return "PRIVATE";
    }

    public static String nullSingleton() {
        return null;
    }

    public String notSingleton() {
        return privateSingleton();
    }

    private ProtectedType() {}
}
