package net.morimekta.proto.test.utils;

import com.google.protobuf.Duration;
import com.google.protobuf.Timestamp;
import org.junit.jupiter.api.Test;

import java.time.Instant;
import java.time.temporal.ChronoUnit;

import static java.time.Duration.ofMillis;
import static java.time.Instant.ofEpochMilli;
import static java.util.concurrent.TimeUnit.DAYS;
import static java.util.concurrent.TimeUnit.HOURS;
import static java.util.concurrent.TimeUnit.MICROSECONDS;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.MINUTES;
import static java.util.concurrent.TimeUnit.NANOSECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static net.morimekta.proto.utils.GoogleTypesUtil.displayableDuration;
import static net.morimekta.proto.utils.GoogleTypesUtil.displayableTimestamp;
import static net.morimekta.proto.utils.GoogleTypesUtil.makeProtoDuration;
import static net.morimekta.proto.utils.GoogleTypesUtil.makeProtoTimestamp;
import static net.morimekta.proto.utils.GoogleTypesUtil.parseDurationString;
import static net.morimekta.proto.utils.GoogleTypesUtil.parseTimestampString;
import static net.morimekta.proto.utils.GoogleTypesUtil.simpleDurationString;
import static net.morimekta.proto.utils.GoogleTypesUtil.toEpochMillis;
import static net.morimekta.proto.utils.GoogleTypesUtil.toEpochSeconds;
import static net.morimekta.proto.utils.GoogleTypesUtil.toInstant;
import static net.morimekta.proto.utils.GoogleTypesUtil.toInstantOrNull;
import static net.morimekta.proto.utils.GoogleTypesUtil.toJavaDuration;
import static net.morimekta.proto.utils.GoogleTypesUtil.toJavaDurationOrNull;
import static net.morimekta.proto.utils.GoogleTypesUtil.toProtoDuration;
import static net.morimekta.proto.utils.GoogleTypesUtil.toProtoTimestamp;
import static net.morimekta.proto.utils.GoogleTypesUtil.truncateTimestamp;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class GoogleTypesUtilTest {
    @Test
    public void testTimestamp() {
        Timestamp ts = Timestamp.newBuilder()
                                .setSeconds(123)
                                .setNanos(456000000)
                                .build();

        assertThat(toInstantOrNull(null), is(nullValue()));
        assertThat(toInstantOrNull(Timestamp.getDefaultInstance()), is(nullValue()));
        assertThat(toInstant(null), is(Instant.EPOCH));
        assertThat(toInstant(Timestamp.getDefaultInstance()), is(Instant.EPOCH));
        assertThat(toInstant(ts), is(ofEpochMilli(123456)));
        assertThat(toEpochMillis(ts), is(123456L));
        assertThat(toEpochSeconds(ts), is(123L));
        assertThat(toEpochSeconds(null), is(0L));
        assertThat(toProtoTimestamp(toInstant(ts)), is(ts));
        assertThat(toProtoTimestamp(null), is(Timestamp.getDefaultInstance()));
        assertThat(makeProtoTimestamp(123456, MILLISECONDS), is(ts));
        assertThat(makeProtoTimestamp(0, MILLISECONDS), is(Timestamp.getDefaultInstance()));

        assertThat(displayableTimestamp(null), is("null"));
        assertThat(displayableTimestamp(toProtoTimestamp(ofEpochMilli(1234567890000L))), is("2009-02-13 23:31:30 UTC"));

        Timestamp tsSeconds = Timestamp.newBuilder()
                                       .setSeconds(123)
                                       .build();
        assertThat(truncateTimestamp(ts, ChronoUnit.SECONDS), is(tsSeconds));

        assertThat(parseTimestampString("2009-02-14"),
                   is(makeProtoTimestamp(1234569600, SECONDS)));
        assertThat(parseTimestampString("2009-02-13 23:31:30"),
                   is(makeProtoTimestamp(1234567890, SECONDS)));
        assertThat(parseTimestampString("2009-02-13 23:31:30.123"),
                   is(makeProtoTimestamp(1234567890123L, MILLISECONDS)));
        assertThat(parseTimestampString("2009-02-13 23:31:30.123 UTC"),
                   is(makeProtoTimestamp(1234567890123L, MILLISECONDS)));
        assertThat(parseTimestampString("2009-02-13T23:31:30.123Z"),
                   is(makeProtoTimestamp(1234567890123L, MILLISECONDS)));

        try {
            fail("No exception: " + parseTimestampString("foo"));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Expected ISO date format, but found: \"foo\""));
        }
    }

    @Test
    public void testDuration() {
        Duration duration = Duration.newBuilder()
                                    .setSeconds(123)
                                    .setNanos(456000000)
                                    .build();
        assertThat(toJavaDurationOrNull(null), is(nullValue()));
        assertThat(toJavaDurationOrNull(Duration.getDefaultInstance()), is(nullValue()));
        assertThat(toJavaDuration(null), is(java.time.Duration.ZERO));
        assertThat(toJavaDuration(Duration.getDefaultInstance()), is(java.time.Duration.ZERO));
        assertThat(toJavaDuration(duration), is(ofMillis(123456)));
        assertThat(toProtoDuration(ofMillis(123456)), is(duration));
        assertThat(toProtoDuration(ofMillis(0)), is(Duration.getDefaultInstance()));
        assertThat(toProtoDuration(null), is(Duration.getDefaultInstance()));
        assertThat(makeProtoDuration(123456, MILLISECONDS), is(duration));

        assertThat(displayableDuration(null), is("null"));
        assertThat(displayableDuration(makeProtoDuration(123, MILLISECONDS)), is("0.123 sec"));
        assertThat(toJavaDuration(makeProtoDuration(-123, MILLISECONDS)), is(ofMillis(-123)));
        assertThat(toJavaDuration(makeProtoDuration(-123456, MILLISECONDS)), is(ofMillis(-123456)));
        assertThat(toProtoDuration(ofMillis(-123)), is(makeProtoDuration(-123, MILLISECONDS)));
        assertThat(toProtoDuration(ofMillis(-123456)), is(makeProtoDuration(-123456, MILLISECONDS)));
        assertThat(toJavaDuration(Duration.newBuilder().setNanos(-123000000).build()), is(ofMillis(-123)));
        assertThat(toJavaDurationOrNull(Duration.newBuilder().setNanos(-123000000).build()), is(ofMillis(-123)));

        assertThat(simpleDurationString(makeProtoDuration(0, SECONDS)), is("0s"));
        assertThat(simpleDurationString(makeProtoDuration(-123, SECONDS)), is("-123s"));
        assertThat(simpleDurationString(makeProtoDuration(123, MILLISECONDS)), is("0.123s"));
        assertThat(simpleDurationString(makeProtoDuration(123, NANOSECONDS)), is("0.000000123s"));
        assertThat(simpleDurationString(makeProtoDuration(23 * 7, DAYS)), is("13910400s"));
        assertThat(simpleDurationString(makeProtoDuration(123, DAYS)), is("10627200s"));
        assertThat(simpleDurationString(makeProtoDuration(23, HOURS)), is("82800s"));
        assertThat(simpleDurationString(makeProtoDuration(123, MINUTES)), is("7380s"));
        assertThat(simpleDurationString(makeProtoDuration(123, SECONDS)), is("123s"));
        assertThat(simpleDurationString(makeProtoDuration(123123, MILLISECONDS)), is("123.123s"));
        assertThat(simpleDurationString(makeProtoDuration(123123123, MILLISECONDS)), is("123123.123s"));

        assertThat(parseDurationString("123ns"), is(makeProtoDuration(123, NANOSECONDS)));
        assertThat(parseDurationString("123ms"), is(makeProtoDuration(123, MILLISECONDS)));
        assertThat(parseDurationString("123s"), is(makeProtoDuration(123, SECONDS)));
        assertThat(parseDurationString("123m"), is(makeProtoDuration(123, MINUTES)));
        assertThat(parseDurationString("123h"), is(makeProtoDuration(123, HOURS)));
        assertThat(parseDurationString("123h 44s"), is(makeProtoDuration(442844, SECONDS)));
        assertThat(parseDurationString("123d"), is(makeProtoDuration(123, DAYS)));
        assertThat(parseDurationString("123w"), is(makeProtoDuration(123 * 7, DAYS)));
        assertThat(parseDurationString("123.123s"), is(makeProtoDuration(123123, MILLISECONDS)));
        assertThat(parseDurationString("123.123123s"), is(makeProtoDuration(123123123, MICROSECONDS)));
        assertThat(parseDurationString("123.123123123s"), is(makeProtoDuration(123123123123L, NANOSECONDS)));

        try {
            fail("" + parseDurationString("123"));
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Expected duration string, but found: \"123\""));
        }
    }
}
