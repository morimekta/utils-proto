package net.morimekta.proto.test.sio;

import com.google.protobuf.ByteString;
import net.morimekta.proto.sio.ProtoTextMessageReader;
import net.morimekta.proto.sio.ProtoTextMessageWriter;
import net.morimekta.proto.test.TestFields;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoTextMessageIOTest {
    @Test
    public void testProtoText() throws IOException {
        StringWriter out = new StringWriter();
        TestFields.DefaultMessage dm = TestFields.DefaultMessage
                .newBuilder()
                .setBin(ByteString.copyFromUtf8("foo"))
                .build();
        new ProtoTextMessageWriter(out).write(dm);

        assertThat(out.getBuffer().toString(), is("bin: \"foo\"\n"));

        StringReader in = new StringReader(out.getBuffer().toString());

        TestFields.DefaultMessage res = (TestFields.DefaultMessage) new ProtoTextMessageReader(in)
                .read(TestFields.DefaultMessage.getDescriptor());

        assertThat(res, is(dm));
    }
}
