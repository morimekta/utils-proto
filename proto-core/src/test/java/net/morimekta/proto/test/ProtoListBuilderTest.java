package net.morimekta.proto.test;

import net.morimekta.proto.ProtoList;
import net.morimekta.proto.ProtoListBuilder;
import org.junit.jupiter.api.Test;

import java.util.Comparator;
import java.util.List;
import java.util.ListIterator;
import java.util.NoSuchElementException;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ProtoListBuilderTest {
    @Test
    public void testList() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .addAllStrings(listOf("foo", "bar"));
        var list = new ProtoListBuilder<String>(msg, msg.getDescriptorForType().findFieldByName("strings"));
        assertThat(list.toString(), is("[\"foo\", \"bar\"]"));

        list.add("foo");
        list.remove(0);
        list.addAll(1, listOf("bar", "baz"));
        list.add(0, "foo2");
        list.set(1, "baa");
        assertThat(list, is(listOf("foo2", "baa", "bar", "baz", "foo")));

        list.clear();
        list.addAll(listOf("gob", "baz"));
        list.set(2, "bar");
        list.sort(Comparator.naturalOrder());
        assertThat(list, is(listOf("bar", "baz", "gob")));

        ListIterator<String> li14 = list.listIterator();
        assertThat(li14.next(), is("bar"));
        li14.remove();
        assertThat(li14.next(), is("baz"));
        li14.set("foo");
        li14.add("foo2");
        assertThat(li14.nextIndex(), is(2));
        assertThat(li14.previousIndex(), is(1));

        assertThat(li14.next(), is("gob"));

        assertThat(list.lastIndexOf("foo"), is(1));
        assertThat(list.lastIndexOf("boo"), is(-1));
        assertThat(list.addAll(2, listOf()), is(false));
        assertThat(list, is(List.of("foo2", "foo", "gob")));

        list.clear();
        assertThat(list, is(listOf()));

        list.add("moo");
        assertThat(list, is(listOf("moo")));
        list.remove(0);
        assertThat(list, is(listOf()));

        list.addAll(0, listOf("foo", "bar"));
        assertThat(list, is(List.of("foo", "bar")));

        var imm = new ProtoList<>(msg.build(), msg.getDescriptorForType().findFieldByName("strings"));
        assertThat(imm, is(list));
        assertThat(list, is(imm));
    }

    // TODO: Test enum list.

    @Test
    public void testListFails() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .addAllStrings(listOf("foo", "bar"));
        try {
            new ProtoListBuilder<String>(msg, msg.getDescriptorForType().findFieldByName("sLong"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a list type: test.DefaultMessage.sLong"));
        }
        try {
            new ProtoListBuilder<String>(msg, msg.getDescriptorForType().findFieldByName("string_map"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a list type: test.DefaultMessage.string_map"));
        }

        var list = new ProtoListBuilder<String>(msg, msg.getDescriptorForType().findFieldByName("strings"));
        try {
            list.set(-1, "foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index < 0"));
        }
        try {
            list.set(3, "foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index > size(2)"));
        }
        try {
            list.add(-1, "foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index < 0"));
        }
        try {
            list.add(3, "foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index > size(2)"));
        }
        try {
            list.addAll(-1, listOf("foo"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index < 0"));
        }
        try {
            list.addAll(3, listOf("foo"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index > size(2)"));
        }
        try {
            list.remove(-1);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index < 0"));
        }
        try {
            list.remove(2);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index >= size(2)"));
        }
    }

    @Test
    public void testIterator() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .addAllStrings(listOf("foo", "bar"));
        var list = new ProtoListBuilder<String>(msg, msg.getDescriptorForType().findFieldByName("strings"));

        try {
            list.listIterator(-1);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index < 0"));
        }
        try {
            list.listIterator(3);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("index > size(2)"));
        }

        var values = list.listIterator();
        try {
            values.previous();
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("0 <= 0"));
        }
        assertThat(values.next(), is("foo"));
        values.remove();
        try {
            values.remove();
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("No current item"));
        }
        try {
            values.set("blah");
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("No current item"));
        }
        try {
            values.add("boo");
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("No current item"));
        }
        assertThat(values.next(), is("bar"));
        try {
            values.next();
            fail("no exception");
        } catch (NoSuchElementException e) {
            assertThat(e.getMessage(), is("1 >= 1"));
        }
    }

}
