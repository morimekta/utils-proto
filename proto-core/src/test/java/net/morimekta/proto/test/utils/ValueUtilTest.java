package net.morimekta.proto.test.utils;

import com.google.protobuf.ByteString;
import com.google.protobuf.Message;
import net.morimekta.proto.test.FileMessage;
import net.morimekta.proto.test.TestFields;
import net.morimekta.proto.test.TestFiles;
import net.morimekta.proto.test.TestOuter;
import net.morimekta.proto.test.TestOuter.TestEnum;
import net.morimekta.proto.test.TestOuter.TestEnum2;
import net.morimekta.proto.test.TestOuter.TestMessage;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.proto.utils.ValueUtil.asString;
import static net.morimekta.proto.utils.ValueUtil.defaultToNull;
import static net.morimekta.proto.utils.ValueUtil.isDefault;
import static net.morimekta.proto.utils.ValueUtil.isNotDefault;
import static net.morimekta.proto.utils.ValueUtil.isNullOrDefault;
import static net.morimekta.proto.utils.ValueUtil.toDebugString;
import static net.morimekta.proto.utils.ValueUtil.toProtoValue;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ValueUtilTest {
    @Test
    public void testDefaults() {
        var defaultValue = TestMessage.newBuilder().build();
        var nonDefault = TestMessage.newBuilder().setEnum(TestEnum.FIFTH).build();

        assertThat(isDefault(defaultValue), is(true));
        assertThat(isDefault(nonDefault), is(false));
        try {
            fail("No exception on null input: " + isDefault(null));
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("message == null"));
        }

        assertThat(isNotDefault(defaultValue), is(false));
        assertThat(isNotDefault(nonDefault), is(true));
        assertThat(isNotDefault(null), is(false));

        assertThat(isNullOrDefault(defaultValue), is(true));
        assertThat(isNullOrDefault(nonDefault), is(false));
        assertThat(isNullOrDefault(null), is(true));

        assertThat(defaultToNull(nonDefault), is(nonDefault));
        assertThat(defaultToNull(defaultValue), is(nullValue()));
        assertThat(defaultToNull(null), is(nullValue()));
    }

    @Test
    public void testAsString() {
        assertThat(asString(null), is("null"));
        assertThat(asString(Optional.empty()), is("null"));
        assertThat(asString("foo"), is("\"foo\""));
        assertThat(asString(Optional.of("foo")), is("\"foo\""));
        assertThat(asString(TestMessage
                                    .newBuilder()
                                    .setString("foo")
                                    .build()), is("{string=\"foo\"}"));
        assertThat(asString(TestEnum2.other), is("other"));
        assertThat(asString(mapOf(TestEnum.second, "foo", TestEnum.THIRD, "bar")),
                   is("{second: \"foo\", THIRD: \"bar\"}"));
        assertThat(asString(listOf(TestEnum.second, TestEnum.THIRD)),
                   is("[second, THIRD]"));
    }

    public static Stream<Arguments> argsToDebugString() {
        return Stream.of(
                arguments(TestFields.DefaultMessage
                                  .newBuilder()
                                  .setBin(ByteString.copyFromUtf8("fizz"))
                                  .putAllStringMap(mapOf(
                                          1, "foo",
                                          2, "buzz"))
                                  .addAllStrings(List.of("bar"))
                                  .addAllMessages(List.of(TestFields.NormalMessage
                                                                  .newBuilder()
                                                                  .setI(42)
                                                                  .build()))
                                  .addAllFibList(List.of(
                                          TestFields.Fibonacci.FIRST,
                                          TestFields.Fibonacci.SECOND,
                                          TestFields.Fibonacci.THIRD))
                                  .build(), "" +
                                            "test.DefaultMessage{\n" +
                                            "  bin = b64(Zml6eg)\n" +
                                            "  strings = [\n" +
                                            "    \"bar\"\n" +
                                            "  ]\n" +
                                            "  messages = [\n" +
                                            "    {\n" +
                                            "      i = 42\n" +
                                            "    }\n" +
                                            "  ]\n" +
                                            "  fib_list = [FIRST, SECOND, THIRD]\n" +
                                            "  string_map = {\n" +
                                            "    1: \"foo\",\n" +
                                            "    2: \"buzz\"\n" +
                                            "  }\n" +
                                            "}"),
                arguments(TestOuter.TestMessage3
                                  .newBuilder()
                                  .setExtension(TestFiles.extended, FileMessage
                                          .newBuilder()
                                          .setNested(FileMessage.FileNested
                                                             .newBuilder()
                                                             .setI(1)
                                                             .build())
                                          .build())
                                  .build(), "" +
                                            "test.v2.TestMessage3{\n" +
                                            "  (test.files.extended) = {\n" +
                                            "    nested = {\n" +
                                            "      i = 1\n" +
                                            "    }\n" +
                                            "  }\n" +
                                            "}"),
                arguments(TestMessage.newBuilder().build(), "test.v2.TestMessage{}"));
    }

    @MethodSource("argsToDebugString")
    @ParameterizedTest
    public void testToDebugString(Message message, String debugString) {
        assertThat(toDebugString(message), is(debugString));
    }

    @Test
    public void testGetProtoValue() {
        assertThat(toProtoValue(null, null), is(nullValue()));
    }
}
