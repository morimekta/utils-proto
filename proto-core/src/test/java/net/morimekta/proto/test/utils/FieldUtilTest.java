package net.morimekta.proto.test.utils;

import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import net.morimekta.proto.ProtoMessage;
import net.morimekta.proto.test.TestFields;
import net.morimekta.proto.test.TestFields.DefaultMessage;
import net.morimekta.proto.test.TestFields.DefaultMessage.CompactMessage;
import net.morimekta.proto.test.TestFiles;
import net.morimekta.proto.test.TestOuter.TestMessage;
import net.morimekta.proto.test.TestOuter.TestMessage3;
import net.morimekta.proto.utils.FieldUtil;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import static java.lang.Boolean.FALSE;
import static java.util.Objects.requireNonNull;
import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.proto.utils.FieldUtil.filterFields;
import static net.morimekta.proto.utils.FieldUtil.getDefaultTypeValue;
import static net.morimekta.proto.utils.FieldUtil.getMapKeyDescriptor;
import static net.morimekta.proto.utils.FieldUtil.getMapValueDescriptor;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class FieldUtilTest {
    Descriptors.Descriptor tmd  = ProtoMessage.getMessageDescriptor(TestMessage.class);
    Descriptors.Descriptor tm3d = ProtoMessage.getMessageDescriptor(TestMessage3.class);

    Descriptors.Descriptor dmd = ProtoMessage.getMessageDescriptor(DefaultMessage.class);
    Descriptors.Descriptor cmd = ProtoMessage.getMessageDescriptor(CompactMessage.class);

    private Descriptors.FieldDescriptor field(String name) {
        return TestMessage.getDescriptor().findFieldByName(name);
    }

    private Descriptors.FieldDescriptor dField(String name) {
        return requireNonNull(dmd.findFieldByName(name), "field:" + name + " == null");
    }

    @Test
    public void testMapDescriptors() {
        try {
            getMapKeyDescriptor(field("long_list"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a map field: test.v2.TestMessage.long_list"));
        }
        try {
            getMapValueDescriptor(field("string"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a map field: test.v2.TestMessage.string"));
        }
    }

    @Test
    public void testDefaultTypeValue() {
        assertThat(getDefaultTypeValue(dField("string_map")), is(mapOf()));
        assertThat(getDefaultTypeValue(dField("strings")), is(listOf()));
        assertThat(getDefaultTypeValue(dField("compact")), is(CompactMessage.getDefaultInstance()));
        assertThat(getDefaultTypeValue(dField("fib")), is(TestFields.Fibonacci.NONE));
        assertThat(getDefaultTypeValue(field("enum")), is(nullValue()));  // enum v2 has no default value.
        assertThat(getDefaultTypeValue(dField("str")), is(""));
        assertThat(getDefaultTypeValue(dField("int")), is(0));
        assertThat(getDefaultTypeValue(dField("long")), is(0L));
        assertThat(getDefaultTypeValue(dField("flt")), is(0.0f));
        assertThat(getDefaultTypeValue(dField("dbl")), is(0.0d));
        assertThat(getDefaultTypeValue(dField("boolean")), is(FALSE));
        assertThat(getDefaultTypeValue(dField("bin")), is(ByteString.EMPTY));
    }

    @Test
    public void testFieldPath() {
        MatcherAssert.assertThat(FieldUtil.fieldPathToFields(tmd, "int"), is(listOf(tmd.findFieldByName("int"))));
        MatcherAssert.assertThat(FieldUtil.fieldPathToFields(dmd, "sc_map.i"), is(listOf(
                dmd.findFieldByName("sc_map"),
                cmd.findFieldByName("i"))));
        MatcherAssert.assertThat(FieldUtil.fieldPath(listOf(tmd.findFieldByName("enum"))), is("enum"));
        MatcherAssert.assertThat(FieldUtil.fieldPathAppend("", tmd.findFieldByName("enum")), is("enum"));
        MatcherAssert.assertThat(FieldUtil.fieldPathAppend("foo", tmd.findFieldByName("string")), is("foo.string"));
    }

    @Test
    public void testFieldPathToFields_badArgs() {
        try {
            FieldUtil.fieldPathToFields(null, "foo");
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("rootDescriptor == null"));
        }
        try {
            FieldUtil.fieldPathToFields(tmd, null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("path == null"));
        }
        try {
            FieldUtil.fieldPathToFields(tmd, "");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty path"));
        }
        try {
            FieldUtil.fieldPathToFields(tmd, "null");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Message test.v2.TestMessage has no field named null"));
        }
        try {
            FieldUtil.fieldPathToFields(tmd, ".foo");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty field name in '.foo'"));
        }
        try {
            FieldUtil.fieldPathToFields(tmd, "foo.bar");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Message test.v2.TestMessage has no field named foo"));
        }
        try {
            FieldUtil.fieldPathToFields(tmd, "int.bar");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Intermediate field 'test.v2.TestMessage.int' is not a message"));
        }
        try {
            FieldUtil.fieldPathToFields(dmd, "enum_map.value");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(),
                       is("Intermediate map field 'test.DefaultMessage.enum_map' does not have message value type"));
        }
        try {
            FieldUtil.fieldPathToFields(tm3d, "msg.");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Empty field name in 'msg.'"));
        }
    }

    @Test
    public void testFilterFields() {
        assertThat(filterFields(setOf("str"), dField("str")),
                   is(setOf()));
        assertThat(filterFields(setOf("sc_map.i", "normals.l"), dField("sc_map")),
                   is(setOf("i")));
        assertThat(filterFields(setOf("str"), TestFiles.extended.getDescriptor()),
                   is(setOf()));
        assertThat(filterFields(setOf("messages.i"), dField("messages")),
                   is(setOf("i")));
    }
}
