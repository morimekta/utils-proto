package net.morimekta.proto.test.utils;

import com.google.protobuf.Descriptors;
import net.morimekta.proto.test.TestFields.Fibonacci;
import net.morimekta.proto.test.TestOuter.TestEnum;
import net.morimekta.proto.test.TestOuter.TestMessage;
import org.junit.jupiter.api.Test;

import java.util.Objects;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.collect.UnmodifiableSet.setOf;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonEnumMap;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonEnumName;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonEnumNames;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonFieldMap;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonFieldName;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonFieldNames;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class JsonNameUtilTest {
    @Test
    public void testGetJsonEnumMap() {
        assertThat(getJsonEnumMap(TestEnum.class), is(
                mapOf("NO_VALUE", TestEnum.NO_VALUE,
                      "FIRST", TestEnum.FIRST,
                      "second", TestEnum.second,
                      "THIRD", TestEnum.THIRD,
                      "fourth", TestEnum.fourth,
                      "FIFTH", TestEnum.FIFTH,
                      "sixth", TestEnum.sixth)));
        assertThat(getJsonEnumMap(Fibonacci.class), is(
                mapOf("NONE", Fibonacci.NONE,
                      "FIRST", Fibonacci.FIRST,
                      "SECOND", Fibonacci.SECOND,
                      "OTHER", Fibonacci.SECOND,
                      "THIRD", Fibonacci.THIRD,
                      "FOURTH", Fibonacci.FOURTH,
                      "FIFTH", Fibonacci.FIFTH,
                      "SIXTH", Fibonacci.SIXTH,
                      "SEVENTH", Fibonacci.SEVENTH,
                      "EIGHTH", Fibonacci.EIGHTH)));
    }

    private Descriptors.FieldDescriptor field(String name) {
        return Objects.requireNonNull(TestMessage.getDescriptor().findFieldByName(name));
    }

    @Test
    public void testGetJsonFieldMap() {
        assertThat(getJsonFieldMap(TestMessage.class), is(
                mapOf("enum", field("enum"),
                      "int", field("int"),
                      "string", field("string"),
                      "long_list", field("long_list"),
                      "longList", field("long_list"),
                      "enum2", field("enum2"))));
    }

    @Test
    public void testGetJsonEnumName() {
        assertThat(getJsonEnumName(Fibonacci.OTHER), is("SECOND"));
        assertThat(getJsonEnumNames(Fibonacci.SECOND), is(setOf("SECOND", "OTHER")));
    }

    @Test
    public void testGetJsonFieldName() {
        assertThat(getJsonFieldName(field("long_list")), is("long_list"));
        assertThat(getJsonFieldNames(field("long_list")), is(setOf("long_list", "longList")));
    }
}
