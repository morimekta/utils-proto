package net.morimekta.proto.test;

import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import net.morimekta.proto.ProtoMessage;
import net.morimekta.proto.ProtoMessageBuilder;
import net.morimekta.proto.test.TestFields.DefaultMessage;
import net.morimekta.proto.test.TestFields.DefaultMessage.CompactMessage;
import net.morimekta.proto.test.TestFields.Fibonacci;
import net.morimekta.proto.test.TestFields.NormalMessage;
import org.hamcrest.CoreMatchers;
import org.hamcrest.MatcherAssert;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.Optional;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ProtoMessageTest {
    public final DefaultMessage PROTO = DefaultMessage
            .newBuilder()
            .setBoolean(true)
            .setInt(1)
            .setSInt(2)
            .setFInt(3)
            .setLong(4L)
            .setSLong(5L)
            .setFLong(6L)
            .setFlt(7.0F)
            .setDbl(8.0D)
            .setStr("nine")
            .setBin(ByteString.copyFromUtf8("ten"))
            .setFib(Fibonacci.FIFTH)
            .setCompact(CompactMessage
                                .newBuilder()
                                .setI(12)
                                .setL(13L)
                                .build())
            .setNormal(NormalMessage
                               .newBuilder()
                               .setI(14)
                               .setL(15L)
                               .build())
            .addAllStrings(listOf("six", "teen"))
            .addMessages(NormalMessage
                                 .newBuilder()
                                 .setI(17)
                                 .build())
            .addFibList(Fibonacci.FOURTH)
            .putStringMap(18, "boo")
            .putMessageMap(19, NormalMessage
                    .newBuilder()
                    .setL(20L)
                    .build())
            .putEnumMap("21", Fibonacci.FIRST)
            .putScMap("22", CompactMessage.newBuilder().setL(22L).build())
            .putSnMap("23", NormalMessage.newBuilder().setL(23L).build())
            .build();

    @Test
    public void testProtoMessage() {
        var message = new ProtoMessage(PROTO);
        assertThat(message.getMessage(), is(PROTO));
        assertThat(message, is(message));
        assertThat(message, is(new ProtoMessage(PROTO)));
        assertThat(message, is(not(nullValue())));
        assertThat(message, is(not("foo")));
        assertThat(message, is(CoreMatchers.not(new ProtoMessageBuilder(PROTO.toBuilder()))));
        assertThat(message, is(not(new ProtoMessage(PROTO.toBuilder().setStr("baz")))));
        assertThat(message, is(not(new ProtoMessage(CompactMessage.newBuilder().build()))));
    }

    @Test
    @SuppressWarnings("unchecked")
    public void testProtoMessageGet() {
        var message = new ProtoMessage(PROTO);
        assertThat(message.get(field("boolean")), is(PROTO.getBoolean()));
        assertThat(message.get(field("int")), is(PROTO.getInt()));
        assertThat(message.get(field("sInt")), is(PROTO.getSInt()));
        assertThat(message.get(field("fInt")), is(PROTO.getFInt()));
        assertThat(message.get(field("long")), is(PROTO.getLong()));
        assertThat(message.get(field("sLong")), is(PROTO.getSLong()));
        assertThat(message.get(field("fLong")), is(PROTO.getFLong()));
        assertThat(message.get(field("flt")), is(PROTO.getFlt()));
        assertThat(message.get(field("dbl")), is(PROTO.getDbl()));
        assertThat(message.get(field("str")), is(PROTO.getStr()));
        assertThat(message.get(field("bin")), is(PROTO.getBin()));
        assertThat(message.get(field("fib")), is(PROTO.getFib()));
        assertThat(message.get(field("compact")), is(PROTO.getCompact()));
        assertThat(message.get(field("normal")), is(PROTO.getNormal()));

        assertThat((List<String>) message.get(field("strings")), is(PROTO.getStringsList()));
        assertThat(message.get(field("messages")), is(PROTO.getMessagesList()));
        assertThat(message.get(field("fib_list")), is(PROTO.getFibListList()));

        assertThat(message.get(field("string_map")), is(PROTO.getStringMapMap()));
        assertThat(message.get(field("message_map")), is(PROTO.getMessageMapMap()));
        assertThat(message.get(field("enum_map")), is(PROTO.getEnumMapMap()));
        assertThat(message.get(field("enum_map")), is(mapOf("21", Fibonacci.FIRST)));
        assertThat(message.get(field("sc_map")), is(PROTO.getScMapMap()));
        assertThat(message.get(field("sn_map")), is(PROTO.getSnMapMap()));
        try {
            message.get(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("field == null"));
        }
    }

    @Test
    public void testProtoMessageOptional() {
        var message = new ProtoMessage(PROTO);
        assertThat(message.optional(field("boolean")), is(Optional.of(PROTO.getBoolean())));
        assertThat(message.optional(field("int")), is(Optional.of(PROTO.getInt())));
        assertThat(message.optional(field("sInt")), is(Optional.of(PROTO.getSInt())));
        assertThat(message.optional(field("fInt")), is(Optional.of(PROTO.getFInt())));
        assertThat(message.optional(field("long")), is(Optional.of(PROTO.getLong())));
        assertThat(message.optional(field("sLong")), is(Optional.of(PROTO.getSLong())));
        assertThat(message.optional(field("fLong")), is(Optional.of(PROTO.getFLong())));
        assertThat(message.optional(field("flt")), is(Optional.of(PROTO.getFlt())));
        assertThat(message.optional(field("dbl")), is(Optional.of(PROTO.getDbl())));
        assertThat(message.optional(field("str")), is(Optional.of(PROTO.getStr())));
        assertThat(message.optional(field("bin")), is(Optional.of(PROTO.getBin())));
        assertThat(message.optional(field("fib")), is(Optional.of(PROTO.getFib())));
        assertThat(message.optional(field("compact")), is(Optional.of(PROTO.getCompact())));
        assertThat(message.optional(field("normal")), is(Optional.of(PROTO.getNormal())));

        assertThat(message.optional(field("strings")), is(Optional.of(PROTO.getStringsList())));
        assertThat(message.optional(field("messages")), is(Optional.of(PROTO.getMessagesList())));
        assertThat(message.optional(field("fib_list")), is(Optional.of(PROTO.getFibListList())));

        assertThat(message.optional(field("string_map")), is(Optional.of(PROTO.getStringMapMap())));
        assertThat(message.optional(field("message_map")), is(Optional.of(PROTO.getMessageMapMap())));
        assertThat(message.optional(field("enum_map")), is(Optional.of(PROTO.getEnumMapMap())));
        assertThat(message.optional(field("sc_map")), is(Optional.of(PROTO.getScMapMap())));
        assertThat(message.optional(field("sn_map")), is(Optional.of(PROTO.getSnMapMap())));
        try {
            message.optional(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("field == null"));
        }
    }

    @Test
    public void testProtoMessageHas() {
        var message = new ProtoMessage(PROTO);
        assertThat(message.has(field("boolean")), is(true));
        assertThat(message.has(field("int")), is(true));
        assertThat(message.has(field("sInt")), is(true));
        assertThat(message.has(field("fInt")), is(true));
        assertThat(message.has(field("long")), is(true));
        assertThat(message.has(field("sLong")), is(true));
        assertThat(message.has(field("fLong")), is(true));
        assertThat(message.has(field("flt")), is(true));
        assertThat(message.has(field("dbl")), is(true));
        assertThat(message.has(field("str")), is(true));
        assertThat(message.has(field("bin")), is(true));
        assertThat(message.has(field("fib")), is(true));
        assertThat(message.has(field("compact")), is(true));
        assertThat(message.has(field("normal")), is(true));

        assertThat(message.has(field("strings")), is(true));
        assertThat(message.has(field("messages")), is(true));
        assertThat(message.has(field("fib_list")), is(true));

        assertThat(message.has(field("string_map")), is(true));
        assertThat(message.has(field("message_map")), is(true));
        assertThat(message.has(field("enum_map")), is(true));
        assertThat(message.has(field("sc_map")), is(true));
        assertThat(message.has(field("sn_map")), is(true));
        try {
            message.has(null);
            fail("no exception");
        } catch (NullPointerException e) {
            assertThat(e.getMessage(), is("field == null"));
        }
    }

    @Test
    public void testEmptyMessageGet() {
        var empty = new ProtoMessage(DefaultMessage.getDefaultInstance());
        assertThat(empty.get(field("boolean")), is(false));
        assertThat(empty.get(field("int")), is(0));
        assertThat(empty.get(field("sInt")), is(0));
        assertThat(empty.get(field("fInt")), is(0));
        assertThat(empty.get(field("long")), is(0L));
        assertThat(empty.get(field("sLong")), is(0L));
        assertThat(empty.get(field("fLong")), is(0L));
        assertThat(empty.get(field("flt")), is(0.0F));
        assertThat(empty.get(field("dbl")), is(0.0D));
        assertThat(empty.get(field("str")), is(""));
        assertThat(empty.get(field("bin")), is(ByteString.EMPTY));
        assertThat(empty.get(field("fib")), is(Fibonacci.NONE));
        assertThat(empty.get(field("compact")), is(CompactMessage.getDefaultInstance()));
        assertThat(empty.get(field("normal")), is(NormalMessage.getDefaultInstance()));

        assertThat(empty.get(field("strings")), is(listOf()));
        assertThat(empty.get(field("messages")), is(listOf()));
        assertThat(empty.get(field("fib_list")), is(listOf()));

        assertThat(empty.get(field("string_map")), is(mapOf()));
        assertThat(empty.get(field("message_map")), is(mapOf()));
        assertThat(empty.get(field("enum_map")), is(mapOf()));
        assertThat(empty.get(field("sc_map")), is(mapOf()));
        assertThat(empty.get(field("sn_map")), is(mapOf()));
    }

    @Test
    public void testEmptyMessageOptional() {
        var empty = new ProtoMessage(DefaultMessage.getDefaultInstance().toBuilder().build());
        assertThat(empty.optional(field("boolean")), is(Optional.empty()));
        assertThat(empty.optional(field("int")), is(Optional.empty()));
        assertThat(empty.optional(field("sInt")), is(Optional.empty()));
        assertThat(empty.optional(field("fInt")), is(Optional.empty()));
        assertThat(empty.optional(field("long")), is(Optional.empty()));
        assertThat(empty.optional(field("sLong")), is(Optional.empty()));
        assertThat(empty.optional(field("fLong")), is(Optional.empty()));
        assertThat(empty.optional(field("flt")), is(Optional.empty()));
        assertThat(empty.optional(field("dbl")), is(Optional.empty()));
        assertThat(empty.optional(field("str")), is(Optional.empty()));
        assertThat(empty.optional(field("bin")), is(Optional.empty()));
        assertThat(empty.optional(field("fib")), is(Optional.empty()));
        assertThat(empty.optional(field("compact")), is(Optional.empty()));
        assertThat(empty.optional(field("normal")), is(Optional.empty()));

        assertThat(empty.optional(field("strings")), is(Optional.empty()));
        assertThat(empty.optional(field("messages")), is(Optional.empty()));
        assertThat(empty.optional(field("fib_list")), is(Optional.empty()));

        assertThat(empty.optional(field("string_map")), is(Optional.empty()));
        assertThat(empty.optional(field("message_map")), is(Optional.empty()));
        assertThat(empty.optional(field("enum_map")), is(Optional.empty()));
        assertThat(empty.optional(field("sc_map")), is(Optional.empty()));
        assertThat(empty.optional(field("sn_map")), is(Optional.empty()));
    }

    @Test
    public void testEmptyMessageHas() {
        var empty = new ProtoMessage(DefaultMessage.getDefaultInstance().toBuilder().build());
        assertThat(empty.has(field("boolean")), is(false));
        assertThat(empty.has(field("int")), is(false));
        assertThat(empty.has(field("sInt")), is(false));
        assertThat(empty.has(field("fInt")), is(false));
        assertThat(empty.has(field("long")), is(false));
        assertThat(empty.has(field("sLong")), is(false));
        assertThat(empty.has(field("fLong")), is(false));
        assertThat(empty.has(field("flt")), is(false));
        assertThat(empty.has(field("dbl")), is(false));
        assertThat(empty.has(field("str")), is(false));
        assertThat(empty.has(field("bin")), is(false));
        assertThat(empty.has(field("fib")), is(false));
        assertThat(empty.has(field("compact")), is(false));
        assertThat(empty.has(field("normal")), is(false));

        assertThat(empty.has(field("strings")), is(false));
        assertThat(empty.has(field("messages")), is(false));
        assertThat(empty.has(field("fib_list")), is(false));

        assertThat(empty.has(field("string_map")), is(false));
        assertThat(empty.has(field("message_map")), is(false));
        assertThat(empty.has(field("enum_map")), is(false));
        assertThat(empty.has(field("sc_map")), is(false));
        assertThat(empty.has(field("sn_map")), is(false));
    }

    @Test
    public void testToString() {
        assertThat(new ProtoMessage(DefaultMessage.getDefaultInstance()).asString(), is("{}"));
        assertThat(new ProtoMessage(DefaultMessage.getDefaultInstance()).toString(), is("test.DefaultMessage{}"));
        assertThat(new ProtoMessage(PROTO).asString(),
                   is("{" +
                      "boolean=true, int=1, sInt=2, fInt=3, long=4, sLong=5, fLong=6, flt=7, dbl=8, " +
                      "str=\"nine\", bin=dGVu, fib=FIFTH, compact={l=13, i=12}, normal={l=15, i=14}, " +
                      "strings=[\"six\", \"teen\"], messages=[{i=17}], fib_list=[FOURTH], " +
                      "string_map={18: \"boo\"}, message_map={19: {l=20}}, " +
                      "enum_map={\"21\": FIRST}, sc_map={\"22\": {l=22}}, sn_map={\"23\": {l=23}}}"));
        assertThat(new ProtoMessage(PROTO).toString(),
                   is("test.DefaultMessage{" +
                      "boolean=true, int=1, sInt=2, fInt=3, long=4, sLong=5, fLong=6, flt=7, dbl=8, " +
                      "str=\"nine\", bin=dGVu, fib=FIFTH, compact={l=13, i=12}, normal={l=15, i=14}, " +
                      "strings=[\"six\", \"teen\"], messages=[{i=17}], fib_list=[FOURTH], " +
                      "string_map={18: \"boo\"}, message_map={19: {l=20}}, " +
                      "enum_map={\"21\": FIRST}, sc_map={\"22\": {l=22}}, sn_map={\"23\": {l=23}}}"));
    }

    @Test
    public void testEquals() {
        assertThat(new ProtoMessage(PROTO), is(new ProtoMessage(PROTO.toBuilder().build())));
        assertThat(new ProtoMessage(PROTO), is(not(new ProtoMessage(PROTO.toBuilder().clearBin().build()))));
        assertThat(new ProtoMessage(PROTO).hashCode(), is(new ProtoMessage(PROTO.toBuilder().build()).hashCode()));
    }

    @Test
    public void testUtils() {
        assertThat(ProtoMessage.newBuilder(DefaultMessage.class),
                   is(instanceOf(DefaultMessage.Builder.class)));
        assertThat(ProtoMessage.newBuilder(DefaultMessage.getDescriptor()),
                   is(instanceOf(DefaultMessage.Builder.class)));
        MatcherAssert.assertThat(ProtoMessage.newBuilder(TestOuter.TestMessage3.TestMessage2.getDescriptor()),
                                 is(instanceOf(TestOuter.TestMessage3.TestMessage2.Builder.class)));
        assertThat(ProtoMessage.getDefaultInstance(DefaultMessage.class),
                   is(DefaultMessage.getDefaultInstance()));
        assertThat(ProtoMessage.getDefaultInstance(TestOuter.TestMessage3.TestMessage2.class),
                   is(TestOuter.TestMessage3.TestMessage2.getDefaultInstance()));
        try {
            ProtoMessage.getDefaultInstance(Fibonacci.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a typed message: Fibonacci"));
        }
        try {
            ProtoMessage.getMessageDescriptor(Fibonacci.class);
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a typed message: Fibonacci"));
        }
    }

    private static Descriptors.FieldDescriptor field(String name) {
        return DefaultMessage.getDescriptor().findFieldByName(name);
    }
}
