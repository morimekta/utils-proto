package net.morimekta.proto.test.sio;

import com.google.protobuf.ByteString;
import net.morimekta.proto.sio.ProtoMessageReader;
import net.morimekta.proto.sio.ProtoMessageWriter;
import net.morimekta.proto.test.TestFields;
import org.junit.jupiter.api.Test;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoMessageIOTest {
    @Test
    public void testProto() throws IOException {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        TestFields.DefaultMessage dm = TestFields.DefaultMessage
                .newBuilder()
                .setBin(ByteString.copyFromUtf8("foo"))
                .build();
        new ProtoMessageWriter(out).write(dm);

        ByteArrayInputStream in = new ByteArrayInputStream(out.toByteArray());

        TestFields.DefaultMessage res = (TestFields.DefaultMessage) new ProtoMessageReader(in)
                .read(TestFields.DefaultMessage.getDescriptor());

        assertThat(res, is(dm));
    }
}
