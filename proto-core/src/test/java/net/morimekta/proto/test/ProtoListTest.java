package net.morimekta.proto.test;

import net.morimekta.proto.ProtoList;
import org.junit.jupiter.api.Test;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ProtoListTest {
    @Test
    public void testList() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .addAllStrings(listOf("foo", "bar"))
                .build();
        var list = new ProtoList<String>(msg, msg.getDescriptorForType().findFieldByName("strings"));

        assertThat(list, is(listOf("foo", "bar")));
        assertThat(list.toString(), is("[\"foo\", \"bar\"]"));
    }

    @Test
    public void testListFails() {
        var msg = TestFields.DefaultMessage
                .newBuilder()
                .addAllStrings(listOf("foo", "bar"))
                .build();
        try {
            new ProtoList<String>(msg, msg.getDescriptorForType().findFieldByName("sLong"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a list type: test.DefaultMessage.sLong"));
        }
        try {
            new ProtoList<String>(msg, msg.getDescriptorForType().findFieldByName("string_map"));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Not a list type: test.DefaultMessage.string_map"));
        }
    }
}
