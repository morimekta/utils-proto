package net.morimekta.proto.test.utils;

import com.google.protobuf.Descriptors;
import net.morimekta.proto.test.TestFields;
import net.morimekta.proto.test.TestOuter;
import org.junit.jupiter.api.Test;

import java.util.Objects;
import java.util.Optional;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.proto.utils.MessageUtil.getInMessage;
import static net.morimekta.proto.utils.MessageUtil.optionalInMessage;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class MessageUtilTest {
    private Descriptors.FieldDescriptor field(String name) {
        return Objects.requireNonNull(TestFields.DefaultMessage.getDescriptor().findFieldByName(name));
    }

    @Test
    public void testGetInMessage() {
        var message = TestFields.DefaultMessage
                .newBuilder()
                .setFib(TestFields.Fibonacci.SECOND)
                .setNormal(TestFields.NormalMessage
                                   .newBuilder()
                                   .setI(5)
                                   .setL(7)
                                   .build())
                .addStrings("foo")
                .putStringMap(3, "bar")
                .build();
        assertThat(getInMessage(message, "fib"),
                   is(Optional.of(TestFields.Fibonacci.SECOND)));
        assertThat(getInMessage(message, "int"),
                   is(Optional.of(0)));
        assertThat(getInMessage(message, "str"),
                   is(Optional.of("")));
        assertThat(getInMessage(message, "normal"),
                   is(Optional.of(message.getNormal())));
        assertThat(getInMessage(message, "normal.i"),
                   is(Optional.of(5)));
        assertThat(getInMessage(message, "strings"),
                   is(Optional.of(listOf("foo"))));
        assertThat(getInMessage(message, "string_map"),
                   is(Optional.of(mapOf(3, "bar"))));

        try {
            getInMessage(message, listOf());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No fields arguments"));
        }
        try {
            getInMessage(message, "str.bytes");
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Intermediate field 'test.DefaultMessage.str' is not a message"));
        }
        try {
            getInMessage(message, listOf(field("str"), field("bin")));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Intermediate field 'test.DefaultMessage.str' is not a message"));
        }
    }

    @Test
    public void testGetInMessage_V2() {
        var message = TestOuter.TestMessage.newBuilder().build();
        assertThat(getInMessage(message, "enum"),
                   is(Optional.empty()));  // no default on enum.
        assertThat(getInMessage(message, "enum2"),
                   is(Optional.empty()));  // no default on enum.
        assertThat(getInMessage(message, "int"),
                   is(Optional.of(2)));  // uses explicit default value.
    }

    @Test
    public void testOptionalInMessage() {
        var message = TestFields.DefaultMessage
                .newBuilder()
                .setFib(TestFields.Fibonacci.SECOND)
                .setNormal(TestFields.NormalMessage
                                   .newBuilder()
                                   .setI(5)
                                   .build())
                .addStrings("foo")
                .putStringMap(3, "bar")
                .build();
        assertThat(optionalInMessage(null, listOf(field("fib"))),
                   is(Optional.empty()));
        assertThat(optionalInMessage(message, "fib"),
                   is(Optional.of(TestFields.Fibonacci.SECOND)));
        assertThat(optionalInMessage(message, "int"),
                   is(Optional.empty()));
        assertThat(optionalInMessage(message, "str"),
                   is(Optional.empty()));
        assertThat(optionalInMessage(message, "compact"),
                   is(Optional.empty()));
        assertThat(optionalInMessage(message, "compact.i"),
                   is(Optional.empty()));
        assertThat(optionalInMessage(message, "normal"),
                   is(Optional.of(message.getNormal())));
        assertThat(optionalInMessage(message, "normal.i"),
                   is(Optional.of(5)));
        assertThat(optionalInMessage(message, "normal.l"),
                   is(Optional.empty()));
        assertThat(optionalInMessage(message, "strings"),
                   is(Optional.of(listOf("foo"))));
        assertThat(optionalInMessage(message, "string_map"),
                   is(Optional.of(mapOf(3, "bar"))));
        assertThat(optionalInMessage(message, "fib_list"),
                   is(Optional.empty()));

        try {
            optionalInMessage(message, listOf());
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("No fields arguments"));
        }

        try {
            optionalInMessage(message, listOf(field("str"), field("bin")));
            fail("no exception");
        } catch (IllegalArgumentException e) {
            assertThat(e.getMessage(), is("Intermediate field 'test.DefaultMessage.str' is not a message"));
        }
    }
}
