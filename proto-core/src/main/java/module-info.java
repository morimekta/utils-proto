/**
 * Core library for providence.
 */
module net.morimekta.proto {
    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires transitive com.google.protobuf;

    exports net.morimekta.proto;
    exports net.morimekta.proto.utils;
    exports net.morimekta.proto.sio;
}