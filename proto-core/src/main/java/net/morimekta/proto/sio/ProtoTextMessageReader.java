/*
 * Copyright 2022 Proto Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.sio;

import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import com.google.protobuf.TextFormat;
import net.morimekta.proto.ProtoMessage;

import java.io.IOException;
import java.io.Reader;

import static java.util.Objects.requireNonNull;

/**
 * Reader for reading files using the proto text format.
 */
public class ProtoTextMessageReader implements MessageReader {
    private final Reader            reader;
    private final TextFormat.Parser parser;

    /**
     * @param reader Reader to read from.
     */
    public ProtoTextMessageReader(Reader reader) {
        this(reader, false);
    }

    /**
     * @param reader  Reader to read from.
     * @param lenient If messages should be read leniently.
     */
    public ProtoTextMessageReader(Reader reader, boolean lenient) {
        this.reader = requireNonNull(reader, "in == null");
        this.parser = TextFormat.Parser.newBuilder()
                                       .setAllowUnknownExtensions(lenient)
                                       .setAllowUnknownFields(lenient)
                                       .build();
    }

    @Override
    public Message read(Descriptors.Descriptor descriptor) throws IOException {
        requireNonNull(descriptor, "descriptor == null");
        var builder = ProtoMessage.newBuilder(descriptor);
        parser.merge(reader, builder);
        return builder.build();
    }
}
