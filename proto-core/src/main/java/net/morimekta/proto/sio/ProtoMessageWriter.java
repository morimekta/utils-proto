/*
 * Copyright 2022 Proto Utils Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.sio;

import com.google.protobuf.Message;

import java.io.IOException;
import java.io.OutputStream;

import static java.util.Objects.requireNonNull;

/**
 * Writer implementation for writing messages using the default proto binary
 * format.
 */
public class ProtoMessageWriter implements MessageWriter {
    private final OutputStream out;

    /**
     * @param out Output stream to write to.
     */
    public ProtoMessageWriter(OutputStream out) {
        this.out = requireNonNull(out, "out == null");
    }

    @Override
    public void write(Message message) throws IOException {
        requireNonNull(message, "message == null");
        message.writeTo(out);
        out.flush();
    }
}
