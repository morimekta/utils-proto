/*
 * Copyright 2020 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.utils;

import com.google.protobuf.ByteString;

import java.util.Base64;

/**
 * Most of these are pretty trivial methods, but here to vastly simplify generated code when making builder, mutable
 * values or build a message.
 */
public final class ByteStringUtil {
    /**
     * @param binary Byte string data.
     * @return Base64 encoded string of the binary data.
     */
    public static String toBase64(ByteString binary) {
        if (binary == null) {
            return null;
        }
        if (binary.isEmpty()) {
            return "";
        }
        return Base64.getEncoder()
                     .withoutPadding()
                     .encodeToString(binary.toByteArray());
    }

    /**
     * @param binary Byte string data.
     * @return Hex encoded string of the binary data.
     */
    public static String toHexString(ByteString binary) {
        if (binary == null) {
            return null;
        }
        if (binary.isEmpty()) {
            return "";
        }
        StringBuilder builder = new StringBuilder();
        for (byte b : binary.toByteArray()) {
            builder.append(String.format("%02x", ((int) (b)) % 0x100));
        }
        return builder.toString();
    }

    /**
     * @param content Base64 encoded string.
     * @return Byte string of the encoded data.
     */
    public static ByteString fromBase64(String content) {
        if (content == null) {
            return null;
        }
        if (content.isEmpty()) {
            return ByteString.EMPTY;
        }
        return ByteString.copyFrom(Base64.getDecoder().decode(content));
    }

    /**
     * @param content Hex encoded content.
     * @return Byte string of the encoded data.
     */
    public static ByteString fromHexString(CharSequence content) {
        if (content == null) {
            return null;
        }
        if (content.length() == 0) {
            return ByteString.EMPTY;
        }
        byte[] data = new byte[content.length() / 2];
        for (int i = 0; i < data.length; ++i) {
            int pos = i * 2;
            data[i] = Byte.parseByte(content.subSequence(pos, pos + 2).toString(), 16);
        }
        return ByteString.copyFrom(data);
    }

    // --- Private ---

    private ByteStringUtil() {}
}
