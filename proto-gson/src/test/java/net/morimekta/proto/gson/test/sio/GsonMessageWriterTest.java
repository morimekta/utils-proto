package net.morimekta.proto.gson.test.sio;

import net.morimekta.proto.gson.sio.GsonMessageWriter;
import org.junit.jupiter.api.Test;

import java.io.StringWriter;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GsonMessageWriterTest {
    @Test
    public void testWriteFailure() {
        var writer = new GsonMessageWriter(new StringWriter());
        try {
            writer.write(null);
        } catch (Exception e) {
            assertThat(e, is(instanceOf(NullPointerException.class)));
            assertThat(e.getMessage(), is("message == null"));
        }
    }
}
