package net.morimekta.proto.gson.test.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.protobuf.Any;
import com.google.protobuf.Duration;
import com.google.protobuf.Message;
import com.google.protobuf.Timestamp;
import com.google.protobuf.util.JsonFormat;
import net.morimekta.proto.gson.ProtoTypeAdapterFactory;
import net.morimekta.proto.gson.ProtoTypeOptions;
import net.morimekta.proto.gson.test.TestExt;
import net.morimekta.proto.gson.test.TestProto.CompactMessage;
import net.morimekta.proto.gson.test.TestProto.DefaultMessage;
import net.morimekta.proto.gson.test.TestProto.NormalMessage;
import net.morimekta.proto.gson.test.TestV2.SimpleMessage;
import net.morimekta.proto.utils.GoogleTypesUtil;
import net.morimekta.proto.utils.ProtoTypeRegistry;
import net.morimekta.proto.utils.ValueUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.StringReader;
import java.util.concurrent.TimeUnit;
import java.util.stream.Stream;

import static java.time.Duration.ofSeconds;
import static net.morimekta.proto.testing.ProtoMatchers.equalToMessage;
import static net.morimekta.proto.utils.GoogleTypesUtil.makeProtoDuration;
import static net.morimekta.proto.utils.GoogleTypesUtil.makeProtoTimestamp;
import static net.morimekta.proto.utils.GoogleTypesUtil.toProtoDuration;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class ProtoMessageTypeAdapterTest {
    public static Stream<Arguments> testAdapterArgs() {
        return Stream.of(
                arguments(SimpleMessage.newBuilder().setExtension(TestExt.complex, "bar").build(),
                          "{\"test.ext.complex\":\"bar\"}"),
                arguments(DefaultMessage.newBuilder().setStr("foo").build(),
                          "{\"str\":\"foo\"}"));
    }

    @MethodSource("testAdapterArgs")
    @ParameterizedTest
    public void testAdapter(Message message, String json) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(
                new ProtoTypeAdapterFactory(
                        new ProtoTypeOptions()
                                .withRegistry(ProtoTypeRegistry.newBuilder()
                                                               .register(TestExt.getDescriptor())
                                                               .build())));
        Gson gson = builder.create();

        Message test = gson.fromJson(json, message.getClass());
        assertThat(test, equalToMessage(message));

        String serialized = gson.toJson(test);
        assertThat(serialized, is(json));
    }

    public static Stream<Arguments> testAdapterFailsArgs() {
        return Stream.of(
                arguments(DefaultMessage.class, "{\"ts\":\"foo\"}",
                          "Expected ISO date format, but found: \"foo\" at $.ts"),
                arguments(DefaultMessage.class, "{\"ts\":false}",
                          "Expected '{' or '[' or unix timestamp or ISO date or null, but found BOOLEAN at $.ts"),
                arguments(DefaultMessage.class, "{\"dur\":\"foo\"}",
                          "Expected duration string, but found: \"foo\" at $.dur"),
                arguments(DefaultMessage.class, "{\"dur\":false}",
                          "Expected '{' or '[', duration number or string or null, but found BOOLEAN at $.dur"),
                arguments(DefaultMessage.class, "{\"unknown\":\"bar\"}",
                          "Unknown field at $.unknown"),
                arguments(DefaultMessage.class, "{\"normal\":[]}",
                          "Compact format not allowed for NormalMessage at $.normal"),
                arguments(DefaultMessage.class, "{\"int\":\"int\"}",
                          "Invalid int value \"int\" at $.int"),
                arguments(DefaultMessage.class, "{\"long\":\"long\"}",
                          "Invalid long value \"long\" at $.long"),
                arguments(DefaultMessage.class, "{\"flt\":\"flt\"}",
                          "Invalid float value \"flt\" at $.flt"),
                arguments(DefaultMessage.class, "{\"dbl\":\"dbl\"}",
                          "Invalid double value \"dbl\" at $.dbl"),
                arguments(SimpleMessage.class, "[]",
                          "Compact format not allowed for test.v2.SimpleMessage at $"));
    }

    @MethodSource("testAdapterFailsArgs")
    @ParameterizedTest
    public void testAdapterFails(Class<? extends Message> message, String json, String errorMessage) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory(new ProtoTypeOptions()
                                                                               .withEnabled(ProtoTypeOptions.Option.FAIL_ON_UNKNOWN_FIELD)
                                                                               .withRegistry(ProtoTypeRegistry.newBuilder()
                                                                                                              .register(
                                                                                                                      TestExt.getDescriptor())
                                                                                                              .build())));
        Gson gson = builder.create();
        try {
            fail("No exception, but returned: " + ValueUtil.toDebugString(gson.fromJson(json, message)));
        } catch (JsonParseException e) {
            assertThat("JsonParseException message:", e.getMessage(), is(errorMessage));
        }
    }

    public static Stream<Arguments> testTimestampArgs() {
        return Stream.of(
                arguments("{\"ts\":1234567890}", makeProtoTimestamp(1234567890, TimeUnit.SECONDS)),
                arguments("{\"ts\":1234567890.123}", makeProtoTimestamp(1234567890123L, TimeUnit.MILLISECONDS)),
                arguments("{\"ts\":\"2009-02-14\"}", makeProtoTimestamp(1234569600, TimeUnit.SECONDS)),
                arguments("{\"ts\":\"2009-02-13 23:31:30\"}", makeProtoTimestamp(1234567890, TimeUnit.SECONDS)),
                arguments("{\"ts\":\"2009-02-13 23:31:30.123\"}",
                          makeProtoTimestamp(1234567890123L, TimeUnit.MILLISECONDS)),
                arguments("{\"ts\":\"2009-02-13 23:31:30.123 UTC\"}",
                          makeProtoTimestamp(1234567890123L, TimeUnit.MILLISECONDS)),
                arguments("{\"ts\":\"2009-02-13T23:31:30.123Z\"}",
                          makeProtoTimestamp(1234567890123L, TimeUnit.MILLISECONDS)),
                arguments("{\"ts\":{\"seconds\":1234567890}}", makeProtoTimestamp(1234567890, TimeUnit.SECONDS)));
    }

    @MethodSource("testTimestampArgs")
    @ParameterizedTest
    public void testTimestamp(String json, Timestamp expectedTs) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory());
        Gson gson = builder.create();

        DefaultMessage test = gson.fromJson(json, DefaultMessage.class);
        assertThat(test.getTs(), equalToMessage(expectedTs));
    }

    @Test
    public void testWriteTimestampAsIso() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory(new ProtoTypeOptions().withEnabled(
                ProtoTypeOptions.Option.WRITE_TIMESTAMP_AS_ISO)));
        Gson gson = builder.create();

        var json = gson.toJson(DefaultMessage.newBuilder()
                                             .setTs(makeProtoTimestamp(1234569600, TimeUnit.SECONDS))
                                             .build());
        assertThat(json, is("{\"ts\":\"2009-02-14T00:00:00Z\"}"));
        json = gson.toJson(DefaultMessage.newBuilder().setTs(makeProtoTimestamp(1234567890, TimeUnit.SECONDS)).build());
        assertThat(json, is("{\"ts\":\"2009-02-13T23:31:30Z\"}"));
        json = gson.toJson(DefaultMessage.newBuilder()
                                         .setTs(makeProtoTimestamp(1234567890123L, TimeUnit.MILLISECONDS))
                                         .build());
        assertThat(json, is("{\"ts\":\"2009-02-13T23:31:30.123Z\"}"));
    }

    public static Stream<Arguments> testDurationArgs() {
        return Stream.of(
                arguments("{\"dur\":123}", makeProtoDuration(123, TimeUnit.SECONDS)),
                arguments("{\"dur\":123.123}", makeProtoDuration(123123, TimeUnit.MILLISECONDS)),
                arguments("{\"dur\":123.123456}", makeProtoDuration(123123456, TimeUnit.MICROSECONDS)),
                arguments("{\"dur\":\"0s\"}", makeProtoDuration(0, TimeUnit.SECONDS)),
                arguments("{\"dur\":\"123ns\"}", makeProtoDuration(123, TimeUnit.NANOSECONDS)),
                arguments("{\"dur\":\"123ms\"}", makeProtoDuration(123, TimeUnit.MILLISECONDS)),
                arguments("{\"dur\":\"123.123s\"}", makeProtoDuration(123123, TimeUnit.MILLISECONDS)),
                arguments("{\"dur\":\"123s\"}", makeProtoDuration(123, TimeUnit.SECONDS)),
                arguments("{\"dur\":\"-123s\"}", makeProtoDuration(-123, TimeUnit.SECONDS)),
                arguments("{\"dur\":\"-123.123123s\"}", toProtoDuration(ofSeconds(123).plusNanos(123123000).negated())),
                arguments("{\"dur\":\"123m\"}", makeProtoDuration(123, TimeUnit.MINUTES)),
                arguments("{\"dur\":\"23h\"}", makeProtoDuration(23, TimeUnit.HOURS)),
                arguments("{\"dur\":\"123h\"}", makeProtoDuration(123, TimeUnit.HOURS)),
                arguments("{\"dur\":\"123d\"}", makeProtoDuration(123, TimeUnit.DAYS)),
                arguments("{\"dur\":\"40w\"}", makeProtoDuration(40 * 7, TimeUnit.DAYS)),
                arguments("{\"dur\":\"123w\"}", makeProtoDuration(123 * 7, TimeUnit.DAYS)),
                arguments("{\"dur\":{\"nanos\":12300000}}", makeProtoDuration(12300000, TimeUnit.NANOSECONDS)),
                arguments("{\"dur\":{\"seconds\":123}}", makeProtoDuration(123, TimeUnit.SECONDS)),
                arguments("{\"dur\":{\"seconds\":-123}}", makeProtoDuration(-123, TimeUnit.SECONDS)),
                arguments("{\"dur\":{\"seconds\":123123}}", makeProtoDuration(123123, TimeUnit.SECONDS)));
    }

    @MethodSource("testDurationArgs")
    @ParameterizedTest
    public void testDuration(String json, Duration expectedDuration) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory());
        Gson gson = builder.create();

        DefaultMessage test = gson.fromJson(json, DefaultMessage.class);
        assertThat(test.getDur(), equalToMessage(expectedDuration));
    }

    public static Stream<Arguments> testWriteDurationAsStringArgs() {
        return Stream.of(
                arguments("{\"dur\":\"0s\"}", makeProtoDuration(0, TimeUnit.SECONDS)),
                arguments("{\"dur\":\"0.000000123s\"}", makeProtoDuration(123, TimeUnit.NANOSECONDS)),
                arguments("{\"dur\":\"0.0123s\"}", makeProtoDuration(12300000, TimeUnit.NANOSECONDS)),
                arguments("{\"dur\":\"0.123s\"}", makeProtoDuration(123, TimeUnit.MILLISECONDS)),
                arguments("{\"dur\":\"123.123s\"}", makeProtoDuration(123123, TimeUnit.MILLISECONDS)),
                arguments("{\"dur\":\"123s\"}", makeProtoDuration(123, TimeUnit.SECONDS)),
                arguments("{\"dur\":\"7380s\"}", makeProtoDuration(123, TimeUnit.MINUTES)),
                arguments("{\"dur\":\"82800s\"}", makeProtoDuration(23, TimeUnit.HOURS)),
                arguments("{\"dur\":\"10627200s\"}", makeProtoDuration(123, TimeUnit.DAYS)),
                arguments("{\"dur\":\"24192000s\"}", makeProtoDuration(40 * 7, TimeUnit.DAYS)),
                arguments("{\"dur\":\"-123s\"}", makeProtoDuration(-123, TimeUnit.SECONDS)),
                arguments("{\"dur\":\"123123s\"}", makeProtoDuration(123123, TimeUnit.SECONDS)));
    }

    @MethodSource("testWriteDurationAsStringArgs")
    @ParameterizedTest
    public void testReadDurationAsString(String expectedJson, Duration duration) {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory(new ProtoTypeOptions().withEnabled(
                ProtoTypeOptions.Option.WRITE_DURATION_AS_STRING)));
        Gson gson = builder.create();

        var json = gson.toJson(DefaultMessage.newBuilder().setDur(duration).build());
        assertThat(json, is(expectedJson));
    }

    @Test
    public void testJsonFormatCompatibility() throws IOException {
        var fields = DefaultMessage.newBuilder()
                                   .setAny(Any.pack(NormalMessage.newBuilder().setI(42).build()))
                                   .setDur(GoogleTypesUtil.makeProtoDuration(13579, TimeUnit.MILLISECONDS))
                                   .setTs(GoogleTypesUtil.makeProtoTimestamp(1234567890, TimeUnit.SECONDS))
                                   .putBsMap(true, "YAY")
                                   .setDbl(43.21d)
                                   .addCompacts(CompactMessage.newBuilder().setL(42L).build())
                                   .build();

        var registry = JsonFormat.TypeRegistry.newBuilder().add(NormalMessage.getDescriptor()).build();
        var typeRegistry = ProtoTypeRegistry.newBuilder().register(NormalMessage.getDescriptor()).build();

        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory(new ProtoTypeOptions()
                                                                                   .withRegistry(typeRegistry)));
        Gson gson = gsonBuilder.create();

        var jfJson = JsonFormat.printer()
                               .preservingProtoFieldNames()
                               .usingTypeRegistry(registry)
                               .omittingInsignificantWhitespace()
                               .print(fields);
        var upJson = gson.toJson(fields);

        assertThat(gson.fromJson(jfJson, DefaultMessage.class), equalToMessage(fields));

        var builder = DefaultMessage.newBuilder();
        JsonFormat.parser().usingTypeRegistry(registry).merge(new StringReader(upJson), builder);
        assertThat(builder.build(), is(fields));

        // Because of nits in handling some numbers, this will always be a little different.
        // at least it parses
        // assertThat(jfJson, is(upJson));
    }
}
