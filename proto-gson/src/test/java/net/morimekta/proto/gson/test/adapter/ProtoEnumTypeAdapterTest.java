package net.morimekta.proto.gson.test.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSortedMap;
import net.morimekta.proto.gson.ProtoTypeAdapterFactory;
import net.morimekta.proto.gson.ProtoTypeOptions;
import net.morimekta.proto.gson.test.TestProto.Fibonacci;
import org.junit.jupiter.api.Test;

import java.util.Map;
import java.util.TreeMap;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_NULL_VALUE;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_UNKNOWN_ENUM;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_UNKNOWN_FIELD;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.WRITE_ENUM_AS_NUMBER;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ProtoEnumTypeAdapterTest {
    public static class Simple {
        public Fibonacci                                   ev;
        public Map<Fibonacci, Fibonacci>                   map;
        public UnmodifiableSortedMap<Fibonacci, Fibonacci> usMap;
        public TreeMap<Fibonacci, Fibonacci>               sMap;
    }

    @Test
    public void testAdapter() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(
                new ProtoTypeAdapterFactory(
                        new ProtoTypeOptions()
                                .withEnabled(FAIL_ON_NULL_VALUE,
                                             FAIL_ON_UNKNOWN_ENUM,
                                             FAIL_ON_UNKNOWN_FIELD)));
        Gson gson = builder.create();

        Simple test = gson.fromJson("{\"ev\":\"FIRST\",\"map\":{\"SECOND\":\"SECOND\",\"3\":3}}", Simple.class);
        assertThat(test, is(notNullValue()));
        assertThat(test.ev, is(Fibonacci.FIRST));
        assertThat(test.map, is(mapOf(Fibonacci.SECOND, Fibonacci.SECOND, Fibonacci.THIRD, Fibonacci.THIRD)));
        assertThat(test.map, is(instanceOf(UnmodifiableMap.class)));

        String serialized = gson.toJson(test);
        assertThat(serialized, is("{\"ev\":\"FIRST\",\"map\":{\"SECOND\":\"SECOND\",\"THIRD\":\"THIRD\"}}"));

        test = gson.fromJson("{\"usMap\":{\"SECOND\":\"SECOND\"}}", Simple.class);
        assertThat(test.usMap, is(instanceOf(UnmodifiableSortedMap.class)));

        test = gson.fromJson("{\"sMap\":{\"SECOND\":\"SECOND\"}}", Simple.class);
        assertThat(test.sMap, is(instanceOf(TreeMap.class)));

        try {
            gson.fromJson("{\"ev\":\"foo\"}", Simple.class);
            fail("no exception");
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is("Unknown Fibonacci for 'foo' at $.ev"));
        }
        try {
            gson.fromJson("{\"ev\":4}", Simple.class);
            fail("no exception");
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is("Unknown Fibonacci for 4 at $.ev"));
        }
        try {
            gson.fromJson("{\"map\":{\"foo\":\"SECOND\"}}", Simple.class);
            fail("no exception");
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is("Unknown Fibonacci key for 'foo' at $.map.foo"));
        }
        try {
            gson.fromJson("{\"map\":{\"4\":\"SECOND\"}}", Simple.class);
            fail("no exception");
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is("Unknown Fibonacci key for 4 at $.map.4"));
        }
        try {
            gson.fromJson("{\"map\":{\"SECOND\":null}}", Simple.class);
            fail("no exception");
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is("Null map value at $.map.SECOND"));
        }

        builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory(
                new ProtoTypeOptions().withEnabled(WRITE_ENUM_AS_NUMBER)));
        gson = builder.create();

        test = new Simple();
        test.ev = Fibonacci.FOURTH;
        test.map = mapOf(Fibonacci.FIFTH, Fibonacci.SECOND);

        serialized = gson.toJson(test);
        assertThat(serialized, is("{\"ev\":5,\"map\":{\"8\":2}}"));
    }
}
