package net.morimekta.proto.gson.test.sio;

import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.Message;
import net.morimekta.proto.gson.ProtoTypeOptions;
import net.morimekta.proto.gson.sio.GsonMessageReader;
import net.morimekta.proto.gson.sio.GsonMessageWriter;
import net.morimekta.proto.gson.test.TestProto;
import net.morimekta.proto.utils.ProtoTypeRegistry;
import net.morimekta.strings.StringUtil;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableList.listOf;
import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.WRITE_COMPACT_MESSAGE;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.WRITE_ENUM_AS_NUMBER;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.WRITE_FIELD_AS_NUMBER;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.WRITE_UNPACKED_ANY;
import static net.morimekta.proto.gson.ProtoTypeOptions.Value.ANY_TYPE_FIELD_NAME;
import static net.morimekta.proto.gson.ProtoTypeOptions.Value.ANY_TYPE_PREFIX;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class GsonMessageRWTest {
    public static Stream<Arguments> argsGsonRoundTrip() {
        return Stream.of(
                Arguments.arguments(
                        TestProto.DefaultMessage.newBuilder()
                                                .setBoolean(true)
                                                .setBin(ByteString.copyFromUtf8("foo"))
                                                .setFib(TestProto.Fibonacci.FIFTH)
                                                .setCompact(TestProto.CompactMessage.newBuilder()
                                                                                    .setL(5)
                                                                                    .build())
                                                .build(),
                        new ProtoTypeOptions().withEnabled(WRITE_COMPACT_MESSAGE),
                        "{\n" + "  \"boolean\": true,\n" +
                        "  \"bin\": \"Zm9v\",\n" +
                        "  \"fib\": \"FIFTH\",\n" +
                        "  \"compact\": [\n" +
                        "    null,\n" +
                        "    5\n" +
                        "  ]\n" +
                        "}\n"),
                Arguments.arguments(
                        TestProto.DefaultMessage.newBuilder()
                                                .setBin(ByteString.copyFromUtf8("foo"))
                                                .setFib(TestProto.Fibonacci.FIFTH)
                                                .build(),
                        new ProtoTypeOptions().withEnabled(WRITE_FIELD_AS_NUMBER, WRITE_ENUM_AS_NUMBER),
                        "{\n" + "  \"21\": \"Zm9v\",\n" +
                        "  \"22\": 8\n" +
                        "}\n"),
                Arguments.arguments(
                        TestProto.DefaultMessage.newBuilder()
                                                .addAllIntegers(listOf(1, 2, 3))
                                                .build(),
                        new ProtoTypeOptions(),
                        "{\n" + "  \"integers\": [\n" +
                        "    1,\n" +
                        "    2,\n" +
                        "    3\n" +
                        "  ]\n" +
                        "}\n"),
                Arguments.arguments(
                        TestProto.DefaultMessage.newBuilder()
                                                .putAllLiMap(mapOf(42L, 55))
                                                .build(),
                        new ProtoTypeOptions(),
                        "{\n" + "  \"li_map\": {\n" +
                        "    \"42\": 55\n" +
                        "  }\n" +
                        "}\n"),
                Arguments.arguments(
                        TestProto.DefaultMessage.newBuilder()
                                                .setAny(Any.pack(TestProto.NormalMessage.newBuilder().setI(42).build()))
                                                .build(),
                        new ProtoTypeOptions().withEnabled(WRITE_UNPACKED_ANY),
                        "{\n" + "  \"any\": {\n" +
                        "    \"@type\": \"type.googleapis.com/NormalMessage\",\n" +
                        "    \"i\": 42\n" +
                        "  }\n" +
                        "}\n"),
                Arguments.arguments(
                        TestProto.DefaultMessage.newBuilder()
                                                .setAny(Any.pack(TestProto.NormalMessage.newBuilder().setI(42).build()))
                                                .build(),
                        new ProtoTypeOptions()
                                .withEnabled(WRITE_UNPACKED_ANY)
                                .withValue(ANY_TYPE_FIELD_NAME, "__type")
                                .withValue(ANY_TYPE_PREFIX, ""),
                        "{\n" + "  \"any\": {\n" +
                        "    \"__type\": \"NormalMessage\",\n" +
                        "    \"i\": 42\n" +
                        "  }\n" +
                        "}\n"),
                Arguments.arguments(
                        TestProto.DefaultMessage.newBuilder()
                                                .setAny(Any.pack(TestProto.NormalMessage.newBuilder().setI(42).build()))
                                                .build(),
                        new ProtoTypeOptions().withDisabled(WRITE_UNPACKED_ANY),
                        "{\n" + "  \"any\": {\n" +
                        "    \"type_url\": \"type.googleapis.com/NormalMessage\",\n" +
                        "    \"value\": \"IFQ\"\n" +
                        "  }\n" +
                        "}\n"),
                Arguments.arguments(TestProto.DefaultMessage.getDefaultInstance(),
                                    new ProtoTypeOptions(),
                                    "{}\n")
                        );
    }

    @MethodSource("argsGsonRoundTrip")
    @ParameterizedTest
    public void testGsonRoundTrip(Message message, ProtoTypeOptions options, String expected) throws IOException {
        options = options.withRegistry(ProtoTypeRegistry
                                               .newBuilder()
                                               .register(TestProto.getDescriptor())
                                               .build());
        StringWriter out = new StringWriter();
        try (GsonMessageWriter writer = new GsonMessageWriter(out, options, true)) {
            writer.write(message);
        }

        // This is to bypass a bug in gson read buffer, which may run into a never-ending
        // loop of the content is too short.
        String serialized = out.toString();

        try (GsonMessageReader reader = new GsonMessageReader(
                new StringReader(StringUtil.rightPad(serialized, 5)),
                options)) {
            Message parsed = reader.read(message.getDescriptorForType());
            assertThat(parsed, is(message));
        }

        assertThat(serialized, is(expected));
    }
}
