package net.morimekta.proto.gson.test;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.google.protobuf.ProtocolMessageEnum;
import net.morimekta.proto.gson.ProtoTypeAdapterFactory;
import net.morimekta.proto.gson.ProtoTypeOptions;
import net.morimekta.proto.gson.adapter.ProtoEnumTypeAdapter;
import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class ProtoTypeAdapterFactoryTest {
    @Test
    public void testFactory() {
        ProtoTypeAdapterFactory factory = new ProtoTypeAdapterFactory();
        Gson gson = new Gson();

        assertThat(factory.create(gson, TypeToken.get(String.class)),
                   is(nullValue()));
        assertThat(factory.create(gson, TypeToken.get(ProtocolMessageEnum.class)),
                   is(nullValue()));
        assertThat(factory.create(gson, TypeToken.get(TestProto.Fibonacci.class)),
                   is(instanceOf(ProtoEnumTypeAdapter.class)));
    }

    @Test
    public void testOptions() {
        ProtoTypeOptions options1 = new ProtoTypeOptions();
        ProtoTypeOptions options2 = new ProtoTypeOptions();

        assertThat(options1.hashCode(), is(options2.hashCode()));
        assertThat(options1, is(options2));
        assertThat(options1, is(not("foo")));
        assertThat(options1, is(notNullValue()));
    }
}
