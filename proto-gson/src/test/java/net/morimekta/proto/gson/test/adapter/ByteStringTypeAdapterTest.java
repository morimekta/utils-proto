package net.morimekta.proto.gson.test.adapter;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonParseException;
import com.google.protobuf.ByteString;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.proto.gson.ProtoTypeAdapterFactory;
import net.morimekta.proto.gson.ProtoTypeOptions;
import org.junit.jupiter.api.Test;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_NULL_VALUE;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_UNKNOWN_ENUM;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_UNKNOWN_FIELD;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.IGNORE_UNKNOWN_ANY_TYPE;
import static org.hamcrest.CoreMatchers.instanceOf;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;

public class ByteStringTypeAdapterTest {
    public static class Simple {
        public ByteString                              ev;
        public Map<ByteString, ByteString>             map;
        public UnmodifiableMap<ByteString, ByteString> uMap;
        public LinkedHashMap<ByteString, ByteString>   lhMap;
        public HashMap<ByteString, ByteString>         hMap;
    }

    @Test
    public void testAdapter() {
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory());
        Gson gson = builder.create();
        ByteString foo = ByteString.copyFromUtf8("foo");
        ByteString bar = ByteString.copyFromUtf8("bar");

        Simple test = gson.fromJson("{\"ev\":\"Zm9v\",\"map\":{\"Zm9v\":\"YmFy\"}}", Simple.class);
        assertThat(test, is(notNullValue()));
        assertThat(test.ev, is(foo));
        assertThat(test.map, is(mapOf(foo, bar)));
        assertThat(test.map, is(instanceOf(UnmodifiableMap.class)));

        String serialized = gson.toJson(test);
        assertThat(serialized, is("{\"ev\":\"Zm9v\",\"map\":{\"Zm9v\":\"YmFy\"}}"));

        test = gson.fromJson("{\"uMap\":{\"Zm9v\":\"YmFy\"}}", Simple.class);
        assertThat(test.uMap, is(instanceOf(UnmodifiableMap.class)));

        test = gson.fromJson("{\"lhMap\":{\"Zm9v\":\"YmFy\"}}", Simple.class);
        assertThat(test.lhMap, is(instanceOf(LinkedHashMap.class)));

        test = gson.fromJson("{\"hMap\":{\"Zm9v\":\"YmFy\"}}", Simple.class);
        assertThat(test.hMap, is(instanceOf(HashMap.class)));

        try {
            gson.fromJson("{\"ev\":\"not base64 encoded\"}", Simple.class);
            fail("no exception");
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is("Invalid base64 at $.ev: Illegal base64 character 20"));
        }
        try {
            gson.fromJson("{\"map\":{\"not base64 encoded\":null}}", Simple.class);
            fail("no exception");
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is("Invalid base64 at $.map.not base64 encoded: Illegal base64 character 20"));
        }

        builder = new GsonBuilder();
        builder.registerTypeAdapterFactory(new ProtoTypeAdapterFactory(
                new ProtoTypeOptions()
                        .withEnabled(FAIL_ON_NULL_VALUE, FAIL_ON_UNKNOWN_FIELD, FAIL_ON_UNKNOWN_ENUM)
                        .withDisabled(IGNORE_UNKNOWN_ANY_TYPE)
        ));
        gson = builder.create();
        try {
            gson.fromJson("{\"map\":{\"Zm9v\":null}}", Simple.class);
            fail("no exception");
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is("Null map value at $.map.Zm9v"));
        }
    }
}
