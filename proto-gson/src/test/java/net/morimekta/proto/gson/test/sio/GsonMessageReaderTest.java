package net.morimekta.proto.gson.test.sio;

import com.google.gson.JsonParseException;
import com.google.gson.stream.MalformedJsonException;
import com.google.protobuf.Any;
import com.google.protobuf.ByteString;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import net.morimekta.proto.gson.ProtoTypeOptions;
import net.morimekta.proto.gson.sio.GsonMessageReader;
import net.morimekta.proto.gson.test.TestProto;
import net.morimekta.proto.gson.test.TestProto.CompactMessage;
import net.morimekta.proto.gson.test.TestProto.DefaultMessage;
import net.morimekta.proto.gson.test.TestProto.Fibonacci;
import net.morimekta.proto.utils.ProtoTypeRegistry;
import net.morimekta.proto.utils.ValueUtil;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import java.io.IOException;
import java.io.StringReader;
import java.util.stream.Stream;

import static net.morimekta.collect.UnmodifiableMap.mapOf;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_NULL_VALUE;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_UNKNOWN_ENUM;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_UNKNOWN_FIELD;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.IGNORE_UNKNOWN_ANY_TYPE;
import static net.morimekta.proto.gson.ProtoTypeOptions.Value.ANY_TYPE_FIELD_NAME;
import static net.morimekta.proto.gson.ProtoTypeOptions.Value.ANY_TYPE_PREFIX;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.nullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.jupiter.api.Assertions.fail;
import static org.junit.jupiter.params.provider.Arguments.arguments;

public class GsonMessageReaderTest {
    public static Stream<Arguments> readMalformedArgs() {
        return Stream.of(
                arguments("{\"bin\",\"A\"}",
                          false,
                          "Expected ':' at line 1 column 8 path $.bin",
                          DefaultMessage.getDescriptor())
                        );
    }

    @ParameterizedTest
    @MethodSource("readMalformedArgs")
    public void testReadMalformed(String serialized, boolean strict, String message, Descriptors.Descriptor type)
            throws IOException {
        var options = new ProtoTypeOptions();
        if (strict) {
            options = options.withEnabled(FAIL_ON_NULL_VALUE, FAIL_ON_UNKNOWN_ENUM, FAIL_ON_UNKNOWN_FIELD);
        }
        try (var reader = new GsonMessageReader(new StringReader(serialized), options)) {
            var parsed = reader.read(type);
            fail("No exception parsing '" + serialized + "' -> " + ValueUtil.asString(parsed));
        } catch (MalformedJsonException e) {
            assertThat(e.getMessage(), is(message));
        }
    }

    public static Stream<Arguments> readParseFailureArgs() {
        return Stream.of(
                arguments("{\"bin\":\"A\"}",
                          false,
                          "Input byte[] should at least have 2 bytes for base64 bytes at $.bin",
                          DefaultMessage.getDescriptor()),
                arguments("{\"any\":{\"@type\":\"type.googleapis.com/Foobar\",\"bin\":\"Zm9v\"}}",
                          true,
                          "Unknown type type.googleapis.com/Foobar at $.any.@type",
                          DefaultMessage.getDescriptor()),
                arguments("{\"compact\":[null,7,\"bar\"]}",
                          true,
                          "Data after last field at $.compact[2]",
                          DefaultMessage.getDescriptor()),
                arguments("{\"24\":{\"4\":3,\"3\":\"foo\"}}",
                          true,
                          "Unknown field at $.24.3",
                          DefaultMessage.getDescriptor()),
                arguments("{\"compact\":\"foo\"}}",
                          true,
                          "Expected '{' or '[' or null, but found: STRING at $.compact",
                          DefaultMessage.getDescriptor()),
                arguments("{\"compact\":null}",
                          true,
                          "Null value at $.compact",
                          DefaultMessage.getDescriptor()),
                arguments("{\"sc_map\":{\"foo\":null}}",
                          true,
                          "Null value in map at $.sc_map.foo",
                          DefaultMessage.getDescriptor())
                        );
    }

    @ParameterizedTest
    @MethodSource("readParseFailureArgs")
    public void testReadParseFailure(String serialized, boolean strict, String message, Descriptors.Descriptor type)
            throws IOException {
        var options = new ProtoTypeOptions();
        if (strict) {
            options = options.withEnabled(FAIL_ON_NULL_VALUE, FAIL_ON_UNKNOWN_ENUM, FAIL_ON_UNKNOWN_FIELD);
        }
        try (var reader = new GsonMessageReader(new StringReader(serialized), options)) {
            var parsed = reader.read(type);
            fail("No exception parsing '" + serialized + "' -> " + ValueUtil.asString(parsed));
        } catch (JsonParseException e) {
            assertThat(e.getMessage(), is(message));
        }
    }

    public static Stream<Arguments> readArgs() {
        return Stream.of(
                arguments("{\"any\":{\"@type\":\"type.googleapis.com/DefaultMessage\",\"bin\":\"Zm9v\"}}",
                          true,
                          DefaultMessage.newBuilder()
                                        .setAny(Any.pack(DefaultMessage.newBuilder()
                                                                       .setBin(ByteString.copyFromUtf8("foo"))
                                                                       .build()))
                                        .build()),
                arguments("{\"any\":{\"@type\":\"type.googleapis.com/Foobar\",\"bin\":\"Zm9v\"}}",
                          false,
                          DefaultMessage.newBuilder()
                                        .setAny(Any.newBuilder()
                                                   .setTypeUrl("type.googleapis.com/Foobar")
                                                   // no value.
                                                   .build())
                                        .build()),
                arguments("{\"compact\":[null,7]}",
                          true,
                          DefaultMessage.newBuilder()
                                        .setCompact(CompactMessage.newBuilder()
                                                                  .setL(7L)
                                                                  .build())
                                        .build()),
                arguments("{\"boolean\":true}",
                          true,
                          DefaultMessage.newBuilder()
                                        .setBoolean(true)
                                        .build()),
                arguments("{\"dbl\":1234.5678}",
                          true,
                          DefaultMessage.newBuilder()
                                        .setDbl(1234.5678d)
                                        .build()),
                arguments("{\"fib\":5}",
                          true,
                          DefaultMessage.newBuilder()
                                        .setFib(Fibonacci.FOURTH)
                                        .build()),
                arguments("{\"compact\":[null,7,\"bar\"]}",
                          false,
                          DefaultMessage.newBuilder()
                                        .setCompact(CompactMessage.newBuilder()
                                                                  .setL(7L)
                                                                  .build())
                                        .build()),
                arguments("{\"24\":{\"4\":3,\"3\":\"foo\"}}",
                          false,
                          DefaultMessage.newBuilder()
                                        .setNormal(TestProto.NormalMessage.newBuilder()
                                                                          .setI(3)
                                                                          .build())
                                        .build()),
                arguments("{\"scMap\":{\"4\":[3],\"3\":[55]}}",
                          true,
                          DefaultMessage.newBuilder()
                                        .putAllScMap(mapOf(
                                                "4", CompactMessage.newBuilder().setI(3).build(),
                                                "3", CompactMessage.newBuilder().setI(55).build()))
                                        .build()),
                arguments("{\"bs_map\":{\"false\":\"FAL\",\"true\":\"TRU\"}}",
                          true,
                          DefaultMessage.newBuilder()
                                        .putAllBsMap(mapOf(false, "FAL", true, "TRU"))
                                        .build()),
                arguments("{\"if_map\":{\"1234\":1234.5678,\"4321\":8765.4321}}",
                          true,
                          DefaultMessage.newBuilder()
                                        .putAllIfMap(mapOf(1234, 1234.5678f, 4321, 8765.4321f))
                                        .build()),
                arguments("{\"li_map\":{\"1234\":5678,\"4321\":9876}}",
                          true,
                          DefaultMessage.newBuilder()
                                        .putAllLiMap(mapOf(1234L, 5678, 4321L, 9876))
                                        .build()),
                arguments("{\"sc_map\":null,\"fibs\":[\"FIRST\"]}",
                          false,
                          DefaultMessage.newBuilder()
                                        .addFibs(Fibonacci.FIRST)
                                        .build())
                        );
    }

    @MethodSource("readArgs")
    @ParameterizedTest
    public void testRead(String serialized, boolean strict, Message expected) throws IOException {
        var options = new ProtoTypeOptions().withRegistry(ProtoTypeRegistry.newBuilder()
                                                                           .register(TestProto.getDescriptor())
                                                                           .build());
        if (strict) {
            options = options.withEnabled(
                                     FAIL_ON_NULL_VALUE,
                                     FAIL_ON_UNKNOWN_ENUM,
                                     FAIL_ON_UNKNOWN_FIELD)
                             .withValue(ANY_TYPE_PREFIX, "")
                             .withValue(ANY_TYPE_PREFIX, "type.googleapis.com/");
        } else {
            options = options
                    .withEnabled(IGNORE_UNKNOWN_ANY_TYPE)
                    .withDisabled(FAIL_ON_NULL_VALUE)
                    .withValue(ANY_TYPE_FIELD_NAME, "@type")
                    .withDefaultValue(ANY_TYPE_PREFIX);
        }
        try (var reader = new GsonMessageReader(new StringReader(serialized), options)) {
            var parsed = reader.read(expected.getDescriptorForType());
            assertThat(parsed, is(expected));
        }
    }

    @Test
    public void testReadNull() throws IOException {
        // NOTE: If we give the string "null" only, the JSON reader will just hang.
        try (var reader = new GsonMessageReader(new StringReader("null\n"), new ProtoTypeOptions())) {
            assertThat(reader.read(DefaultMessage.getDescriptor()), is(nullValue()));
        }
    }
}
