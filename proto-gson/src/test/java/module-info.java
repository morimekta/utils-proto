module net.morimekta.proto.gson.test {
    requires com.google.gson;
    requires com.google.protobuf.util;
    requires com.google.protobuf;
    requires net.morimekta.collect;
    requires net.morimekta.proto.gson;
    requires net.morimekta.proto.testing;
    requires net.morimekta.proto;
    requires net.morimekta.strings;
    requires org.hamcrest;
    requires org.junit.jupiter.api;
    requires org.junit.jupiter.engine;
    requires org.junit.jupiter.params;

    exports net.morimekta.proto.gson.test;
    exports net.morimekta.proto.gson.test.sio;
    exports net.morimekta.proto.gson.test.adapter;
}