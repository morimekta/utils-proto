/*
 * Copyright 2020 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.gson.adapter;

import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonWriter;
import com.google.protobuf.Descriptors;
import com.google.protobuf.Message;
import net.morimekta.proto.gson.ProtoTypeOptions;
import net.morimekta.proto.gson.sio.GsonMessageReader;
import net.morimekta.proto.gson.sio.GsonMessageWriter;

import java.io.IOException;

import static java.util.Objects.requireNonNull;

/**
 * Adapter for proto messages.
 */
public class ProtoMessageTypeAdapter extends TypeAdapter<Message> {
    private final ProtoTypeOptions       options;
    private final Descriptors.Descriptor descriptor;

    /**
     * @param descriptor Descriptor of message adapted.
     * @param options    Options for adapter.
     */
    public ProtoMessageTypeAdapter(Descriptors.Descriptor descriptor, ProtoTypeOptions options) {
        this.options = options;
        this.descriptor = requireNonNull(descriptor, "descriptor == null");
    }

    @Override
    public void write(JsonWriter out, Message value) throws IOException {
        new GsonMessageWriter(out, options).write(value);
    }

    @Override
    public Message read(JsonReader in) throws IOException {
        return new GsonMessageReader(in, options).read(descriptor);
    }
}
