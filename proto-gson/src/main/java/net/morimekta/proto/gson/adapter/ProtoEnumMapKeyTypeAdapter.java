/*
 * Copyright 2020 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.gson.adapter;

import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.protobuf.ProtocolMessageEnum;
import net.morimekta.proto.ProtoEnum;
import net.morimekta.proto.gson.ProtoTypeOptions;

import java.io.IOException;
import java.util.Map;
import java.util.regex.Pattern;

import static java.util.Objects.requireNonNull;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.WRITE_ENUM_AS_NUMBER;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonEnumMap;
import static net.morimekta.proto.utils.JsonNameUtil.getJsonEnumName;

/**
 * Adapter for proto enum key values.
 *
 * @param <E> The enum type.
 * @param <V> The map value type.
 */
public class ProtoEnumMapKeyTypeAdapter<E extends Enum<E> & ProtocolMessageEnum, V>
        extends MapKeyTypeAdapter<E, V> {
    private final ProtoEnum<E>     descriptor;
    private final Map<String, E>   jsonNames;
    private final ProtoTypeOptions options;

    /**
     * @param descriptor   The proto enum type.
     * @param options      Options for the adapter.
     * @param valueAdapter Adapter for handling the value.
     * @param mapClass     The map type class.
     */
    public ProtoEnumMapKeyTypeAdapter(ProtoEnum<E> descriptor,
                                      ProtoTypeOptions options, TypeAdapter<V> valueAdapter,
                                      Class<? extends Map<?, ?>> mapClass) {
        super(valueAdapter, mapClass, options);
        this.descriptor = requireNonNull(descriptor, "descriptor == null");
        this.jsonNames = getJsonEnumMap(descriptor);
        this.options = options;
    }

    @Override
    protected String getKeyName(E key) {
        if (options.isEnabled(WRITE_ENUM_AS_NUMBER)) {
            return String.valueOf(key.getNumber());
        } else {
            return getJsonEnumName(key);
        }
    }

    @Override
    protected E readKey(JsonReader in) throws IOException {
        String name = in.nextName();
        E key;
        if (ID.matcher(name).matches()) {
            int id = Integer.parseInt(name);
            key = descriptor.findByNumber(id);
            if (key == null) {
                throw new JsonParseException(
                        "Unknown " + descriptor.getTypeName() + " key for " + id + " at " + in.getPath());
            }
        } else {
            key = descriptor.findByName(name);
            if (key == null) {
                key = jsonNames.get(name);
            }
            if (key == null) {
                throw new JsonParseException(
                        "Unknown " + descriptor.getTypeName() + " key for '" + name + "' at " + in.getPath());
            }
        }
        return key;
    }

    private static final Pattern ID = Pattern.compile("^(0|[1-9][0-9]*)$");
}
