/*
 * Copyright 2020 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.gson.adapter;

import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.google.protobuf.ProtocolMessageEnum;
import net.morimekta.proto.ProtoEnum;
import net.morimekta.proto.gson.ProtoTypeOptions;
import net.morimekta.proto.utils.JsonNameUtil;

import java.io.IOException;
import java.util.Map;

import static java.util.Objects.requireNonNull;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_UNKNOWN_ENUM;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.WRITE_ENUM_AS_NUMBER;

/**
 * Adapter for proto enum types.
 *
 * @param <E> The enum type.
 */
public class ProtoEnumTypeAdapter<E extends Enum<E> & ProtocolMessageEnum>
        extends TypeAdapter<E> {
    private final ProtoEnum<E>     descriptor;
    private final Map<String, E>   jsonNames;
    private final ProtoTypeOptions options;

    /**
     * @param descriptor The enum descriptor.
     * @param options    Options for the adapter.
     */
    public ProtoEnumTypeAdapter(ProtoEnum<E> descriptor, ProtoTypeOptions options) {
        this.descriptor = requireNonNull(descriptor, "descriptor == null");
        this.jsonNames = JsonNameUtil.getJsonEnumMap(descriptor);
        this.options = options;
    }

    @Override
    public void write(JsonWriter out, E value) throws IOException {
        if (options.isEnabled(WRITE_ENUM_AS_NUMBER)) {
            out.value(value.getNumber());
        } else {
            out.value(JsonNameUtil.getJsonEnumName(value));
        }
    }

    @Override
    public E read(JsonReader in) throws IOException {
        E value;
        if (in.peek() == JsonToken.NULL) {
            in.nextNull();
            return null;
        } else if (in.peek() == JsonToken.NUMBER) {
            int id = in.nextInt();
            value = descriptor.findByNumber(id);
            if (value == null && options.isEnabled(FAIL_ON_UNKNOWN_ENUM)) {
                throw new JsonParseException("Unknown " + descriptor.getTypeName() + " for " + id + " at " + in.getPath());
            }
        } else {
            String name = in.nextString();
            value = descriptor.findByName(name);
            if (value == null) {
                value = jsonNames.get(name);
            }
            if (value == null && options.isEnabled(FAIL_ON_UNKNOWN_ENUM)) {
                throw new JsonParseException("Unknown " + descriptor.getTypeName() + " for '" + name + "' at " + in.getPath());
            }
        }
        return value;
    }
}
