/*
 * Copyright 2020 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.gson.adapter;

import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.protobuf.ByteString;
import net.morimekta.proto.gson.ProtoTypeOptions;
import net.morimekta.proto.utils.ByteStringUtil;

import java.io.IOException;
import java.util.Map;

/**
 * Adapter for handling byte strings for map keys.
 *
 * @param <V> The value type for the map.
 */
public class ByteStringMapKeyTypeAdapter<V>
        extends MapKeyTypeAdapter<ByteString, V> {
    /**
     * @param options      Options for the adapters.
     * @param valueAdapter The value adapter.
     * @param mapClass     The map class.
     */
    public ByteStringMapKeyTypeAdapter(ProtoTypeOptions options,
                                       TypeAdapter<V> valueAdapter,
                                       Class<? extends Map<?, ?>> mapClass) {
        super(valueAdapter, mapClass, options);
    }

    @Override
    protected String getKeyName(ByteString key) {
        return ByteStringUtil.toBase64(key);
    }

    @Override
    protected ByteString readKey(JsonReader in) throws IOException {
        try {
            return ByteStringUtil.fromBase64(in.nextName());
        } catch (IllegalArgumentException e) {
            throw new JsonParseException("Invalid base64 at " + in.getPath() + ": " + e.getMessage(), e);
        }
    }
}
