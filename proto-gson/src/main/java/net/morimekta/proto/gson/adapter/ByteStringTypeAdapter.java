package net.morimekta.proto.gson.adapter;

import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import com.google.protobuf.ByteString;

import java.io.IOException;

import static net.morimekta.proto.utils.ByteStringUtil.fromBase64;
import static net.morimekta.proto.utils.ByteStringUtil.toBase64;

/**
 * Adapter for handling byte strings.
 */
public class ByteStringTypeAdapter extends TypeAdapter<ByteString> {
    @Override
    public void write(JsonWriter jsonWriter, ByteString bytes) throws IOException {
        jsonWriter.value(toBase64(bytes));
    }

    @Override
    public ByteString read(JsonReader jsonReader) throws IOException {
        if (jsonReader.peek() == JsonToken.NULL) {
            jsonReader.nextNull();
            return null;
        }
        try {
            return fromBase64(jsonReader.nextString());
        } catch (IllegalArgumentException e) {
            throw new JsonParseException(
                    "Invalid base64 at " + jsonReader.getPreviousPath() + ": " + e.getMessage(), e);
        }
    }
}
