/*
 * Copyright 2020 Providence Authors
 *
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements. See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership. The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License. You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package net.morimekta.proto.gson.adapter;

import com.google.gson.JsonParseException;
import com.google.gson.TypeAdapter;
import com.google.gson.stream.JsonReader;
import com.google.gson.stream.JsonToken;
import com.google.gson.stream.JsonWriter;
import net.morimekta.collect.MapBuilder;
import net.morimekta.collect.UnmodifiableMap;
import net.morimekta.collect.UnmodifiableSortedMap;
import net.morimekta.proto.gson.ProtoTypeOptions;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;

import static java.util.Objects.requireNonNull;
import static net.morimekta.proto.gson.ProtoTypeOptions.Option.FAIL_ON_NULL_VALUE;

/**
 * Base class for map adapter.
 *
 * @param <E> The key type.
 * @param <V> The value type.
 */
public abstract class MapKeyTypeAdapter<E, V>
        extends TypeAdapter<Map<E, V>> {
    private final TypeAdapter<V>             valueAdapter;
    private final Class<? extends Map<?, ?>> mapClass;
    private final ProtoTypeOptions           options;

    /**
     * @param valueAdapter The value adapter.
     * @param mapClass     The map class.
     * @param options      Options for the adapter.
     */
    protected MapKeyTypeAdapter(TypeAdapter<V> valueAdapter,
                                Class<? extends Map<?, ?>> mapClass,
                                ProtoTypeOptions options) {
        this.valueAdapter = requireNonNull(valueAdapter, "valueAdapter == null");
        this.mapClass = requireNonNull(mapClass, "mapClass == null");
        this.options = options;
    }

    /**
     * Get the map key for the key value.
     *
     * @param key The key to get map key for.
     * @return The map key string value.
     */
    protected abstract String getKeyName(E key);

    /**
     * Read key value from map. It is expected to read a single name and parse it.
     *
     * @param in The json reader.
     * @return The parsed key value.
     * @throws IOException If unable to read or parse value.
     */
    protected abstract E readKey(JsonReader in) throws IOException;

    @Override
    public void write(JsonWriter out, Map<E, V> value) throws IOException {
        if (value == null) {
            out.nullValue();
        } else {
            out.beginObject();
            for (Map.Entry<E, V> entry : value.entrySet()) {
                out.name(getKeyName(entry.getKey()));
                valueAdapter.write(out, entry.getValue());
            }
            out.endObject();
        }
    }

    @Override
    public Map<E, V> read(JsonReader in) throws IOException {
        in.beginObject();
        MapBuilder<E, V> builder = UnmodifiableMap.newBuilder();
        while (in.peek() != JsonToken.END_OBJECT) {
            E key = requireNonNull(readKey(in), "key == null");
            V value = valueAdapter.read(in);
            if (value == null) {
                if (options.isEnabled(FAIL_ON_NULL_VALUE)) {
                    throw new JsonParseException("Null map value at " + in.getPreviousPath());
                }
            } else {
                builder.put(key, value);
            }
        }
        in.endObject();
        return mapWrapper(builder.build());
    }

    private Map<E, V> mapWrapper(Map<E, V> build) {
        if (LinkedHashMap.class.isAssignableFrom(mapClass)) {
            return new LinkedHashMap<>(build);
        } else if (HashMap.class.isAssignableFrom(mapClass)) {
            return new HashMap<>(build);
        } else if (TreeMap.class.isAssignableFrom(mapClass)) {
            return new TreeMap<>(build);
        } else if (SortedMap.class.isAssignableFrom(mapClass)) {
            return UnmodifiableSortedMap.asSortedMap(build);
        } else {
            return build;
        }
    }
}
