/**
 * Module with gson adapters for proto types.
 */
module net.morimekta.proto.gson {
    requires net.morimekta.collect;
    requires net.morimekta.strings;
    requires com.google.gson;
    requires com.google.protobuf;
    requires net.morimekta.proto;

    exports net.morimekta.proto.gson;
    exports net.morimekta.proto.gson.sio;
    exports net.morimekta.proto.gson.adapter;
}